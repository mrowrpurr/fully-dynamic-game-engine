#pragma once

#include <span>
#include <string_view>
#include <vector>

#include "Execution.h"

namespace FDGE::Console {
    class CommandSpec {
    public:
        inline CommandSpec(std::string_view ns, std::initializer_list<std::string_view>&& names,
                           std::function<void(Execution&)> handler)
                : _ns(ns.data()), _handler(std::move(handler)) {
            for (auto& entry : names) {
                _names.emplace_back(entry.data());
            }
        }

        inline CommandSpec(std::string_view ns, std::span<std::string_view>&& names,
                           std::function<void(Execution&)> handler)
                : _ns(ns.data()), _handler(std::move(handler)) {
            for (auto& entry : names) {
                _names.emplace_back(entry.data());
            }
        }

        inline CommandSpec(std::string_view ns, std::span<const std::string>&& names,
                           std::function<void(Execution&)> handler)
                : _ns(ns.data()), _handler(std::move(handler)) {
            for (auto& entry : names) {
                _names.emplace_back(entry.data());
            }
        }

        template <class Names>
        requires(std::ranges::range<std::string_view>)
        inline CommandSpec(std::string_view ns, Names&& names, std::function<void(Execution&)> handler)
                : _ns(ns.data()), _handler(std::move(handler)) {
            for (auto& entry : names) {
                _names.emplace_back(entry.data());
            }
        }

        template <class Names>
        requires(std::ranges::range<std::string>)
        inline CommandSpec(std::string_view ns, Names&& names, std::function<void(Execution&)> handler)
                : _ns(ns.data()), _handler(std::move(handler)) {
            _names.insert(_names.begin(), names.begin(), names.end());
        }

        template <class Names>
        requires(std::ranges::range<const std::string>)
        inline CommandSpec(std::string_view ns, Names&& names, std::function<void(Execution&)> handler)
                : _ns(ns.data()), _handler(std::move(handler)) {
            _names.insert(_names.begin(), names.begin(), names.end());
        }

        [[nodiscard]] inline std::string_view GetNamespace() const noexcept {
            return _ns;
        }

        [[nodiscard]] inline std::span<const std::string> GetNames() const noexcept {
            return std::span<const std::string>(_names.data(), _names.size());
        }

        [[nodiscard]] inline std::function<void(Execution&)> GetHandler() const noexcept {
            return _handler;
        }

    private:
        std::string _ns;
        std::vector<std::string> _names;
        std::function<void(Execution&)> _handler;
    };
}
