#pragma once

#include <RE/Skyrim.h>

namespace FDGE::Console {
    template <class... FmtArgs>
    static inline void Print(std::string_view fmt, FmtArgs... args) {
        if constexpr (sizeof...(FmtArgs) == 0) {
            RE::ConsoleLog::GetSingleton()->Print(fmt.data());
        } else {
            auto msg = std::format(fmt, args...);
            RE::ConsoleLog::GetSingleton()->Print(msg.c_str());
        }
    }
}
