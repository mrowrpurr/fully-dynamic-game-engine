#pragma once

#include <parallel_hashmap/phmap.h>

#include "../Predicate.h"

namespace FDGE::Dymod::Common {
    class IntPredicate : public Predicate {
    public:
        [[nodiscard]] inline std::vector<std::pair<int32_t, int32_t>>& GetRanges() noexcept {
            return _ranges;
        }

        [[nodiscard]] inline const std::vector<std::pair<int32_t, int32_t>>& GetRanges() const noexcept {
            return _ranges;
        }

    protected:
        virtual std::optional<int32_t> GetValue(const gluino::polymorphic_any& target) const noexcept = 0;

        [[nodiscard]] PredicateResult TestImpl(const gluino::polymorphic_any& target) const noexcept;

    private:
        std::vector<std::pair<int32_t, int32_t>> _ranges;
    };
}
