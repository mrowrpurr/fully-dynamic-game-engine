#pragma once

#include <string>

namespace FDGE::Dymod {
    class DynamicEvent {
    public:
        virtual ~DynamicEvent() noexcept = default;

        [[nodiscard]] inline std::u8string_view GetName() const noexcept {
            return _name;
        }

    protected:
        inline DynamicEvent(std::u8string_view name) noexcept
                : _name(name) {
        }

    private:
        std::u8string_view _name;
    };
}
