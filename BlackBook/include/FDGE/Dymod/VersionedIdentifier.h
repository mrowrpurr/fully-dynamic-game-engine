#pragma once

#include <cstdint>

#include <articuno/articuno.h>
#include <gluino/semantic_version.h>

namespace FDGE::Dymod {
    class VersionedIdentifier {
    public:
        [[nodiscard]] inline std::u8string_view GetID() const noexcept {
            return _id;
        }

        inline void SetID(std::u8string_view id) {
            _id = id.data();
        }

        [[nodiscard]] inline const gluino::semantic_version& GetMinVersion() const noexcept {
            return _minVersion;
        }

        inline void SetMinVersion(gluino::semantic_version minVersion) noexcept {
            _minVersion = std::move(minVersion);
        }

        [[nodiscard]] inline const gluino::semantic_version& GetMaxVersion() const noexcept {
            return _maxVersion;
        }

        inline void SetMaxVersion(gluino::semantic_version maxVersion) noexcept {
            _maxVersion = std::move(maxVersion);
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_id, "id");
            ar <=> articuno::kv(_minVersion, "minVersion");
            ar <=> articuno::kv(_maxVersion, "maxVersion");
        }

        std::u8string _id;
        gluino::semantic_version _minVersion;
        gluino::semantic_version _maxVersion;

        friend class articuno::access;
    };
}
