#pragma once

#include "Node.h"

namespace FDGE::Dymod {
    class Action : public Node {
    public:
        virtual ~Action() noexcept = default;

    protected:
        inline Action() noexcept = default;

    private:
    };
}
