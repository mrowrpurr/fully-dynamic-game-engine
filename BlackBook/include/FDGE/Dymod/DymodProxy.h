#pragma once

#include <FDGE/Config/Proxy.h>
#include <FDGE/Util/FilesystemWatcher.h>

#include "Mod.h"

namespace FDGE::Dymod {
    class DymodProxy : Config::Proxy<Mod> {
    public:
        DymodProxy(const DymodProxy&) = delete;

        static const DymodProxy Create(std::string_view name);

        void StartWatching() const override;

        void StopWatching() const override;

        void Save() const override;

    protected:
        explicit DymodProxy(std::string_view name);

        void Refresh(bool isReloading) const override;

    private:
        std::filesystem::path _directory;
        mutable std::unique_ptr<Util::FilesystemWatcher> _watcher;
    };
}
