#pragma once

#include <filesystem>
#include <iostream>
#include <string_view>

#include <FDGE/Plugin.h>
#include <FDGE/Util/Strings.h>
#include <gluino/autorun.h>
#include <gluino/enumeration.h>
#include <libloaderapi.h>

EXTERN_C IMAGE_DOS_HEADER __ImageBase;

namespace FDGE::Hook {
    namespace detail {
        static inline const std::vector<std::string>& GetModuleNames() noexcept {
            static std::vector<std::string> moduleNames{};
            if (!moduleNames.empty()) {
                return moduleNames;
            }

            auto pluginData = FDGE::detail::Plugin::Lookup();
            if (pluginData) {
                if (pluginData->SerializationModuleNames) {
                    for (auto module: pluginData->SerializationModuleNames) {
                        moduleNames.emplace_back(module.data());
                    }
                }
                if (moduleNames.empty() && pluginData->Name) {
                    const char* pluginName = pluginData->Name;
                    if (pluginName && pluginName[0] != '\0') {
                        moduleNames.emplace_back(pluginName);
                    }
                }
            }
            if (moduleNames.empty()) {
                FDGE_WIN_CHAR_TYPE path[MAX_PATH];
                GetModuleFileName(reinterpret_cast<HINSTANCE>(&__ImageBase), path, MAX_PATH);
                std::filesystem::path modulePath(path);
                moduleNames.emplace_back(modulePath.filename().stem().string());
            }
            return moduleNames;
        }
    }

    gluino_enum(HandlerStage, uint8_t,
                (Post, 0),
                (Pre, 1));

    namespace detail {
        void __declspec(dllexport) RegisterSaveHandler(
                std::string_view moduleName, std::string_view handlerName, HandlerStage stage,
                const std::function<void(std::ostream&)>& handler);

        void __declspec(dllexport) RegisterLoadHandler(
                const std::vector<std::string>& moduleName, std::string_view handlerName, HandlerStage stage,
                const std::function<void(std::istream&)>& handler);

        bool __declspec(dllexport) UnregisterSaveHandler(std::string_view moduleName, std::string_view handlerName,
                                                         HandlerStage stage);

        bool __declspec(dllexport)  UnregisterLoadHandler(const std::vector<std::string>& moduleNames,
                                                          std::string_view handlerName, HandlerStage stage);
    }

    inline void RegisterSaveHandler(std::string_view handlerName, HandlerStage stage, const std::function<void(std::ostream&)>& handler) {
        FDGE::Hook::detail::RegisterSaveHandler(FDGE::Hook::detail::GetModuleNames()[0],
                handlerName, stage, handler);
    }

    inline void RegisterSaveHandler(std::string_view handlerName, HandlerStage stage, const std::function<void(std::ostream&)>&& handler) {
        RegisterSaveHandler(handlerName, stage, handler);
    }

    inline void RegisterSaveHandler(std::string_view handlerName, const std::function<void(std::ostream&)>& handler) {
        RegisterSaveHandler(handlerName, HandlerStage::Post, handler);
    }

    inline void RegisterSaveHandler(std::string_view handlerName, const std::function<void(std::ostream&)>&& handler) {
        RegisterSaveHandler(handlerName, handler);
    }

    inline void RegisterLoadHandler(std::string_view handlerName, HandlerStage stage, const std::function<void(std::istream&)>& handler) {
        FDGE::Hook::detail::RegisterLoadHandler(FDGE::Hook::detail::GetModuleNames(),
                handlerName, stage, handler);
    }

    inline void RegisterLoadHandler(std::string_view handlerName, HandlerStage stage, const std::function<void(std::istream&)>&& handler) {
        RegisterLoadHandler(handlerName, stage, handler);
    }

    inline void RegisterLoadHandler(std::string_view handlerName, const std::function<void(std::istream&)>& handler) {
        RegisterLoadHandler(handlerName, HandlerStage::Post, handler);
    }

    inline void RegisterLoadHandler(std::string_view handlerName, const std::function<void(std::istream&)>&& handler) {
        RegisterLoadHandler(handlerName, handler);
    }

    inline void UnregisterSaveHandler(std::string_view handlerName, HandlerStage stage = HandlerStage::Post) {
        FDGE::Hook::detail::UnregisterSaveHandler(FDGE::Hook::detail::GetModuleNames()[0],
                handlerName, stage);
    }

    inline void UnregisterLoadHandler(std::string_view handlerName, HandlerStage stage = HandlerStage::Post) {
        FDGE::Hook::detail::UnregisterLoadHandler(FDGE::Hook::detail::GetModuleNames(),
                handlerName, stage);
    }
}
