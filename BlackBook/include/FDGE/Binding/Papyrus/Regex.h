#pragma once

#include "ScriptObject.h"

namespace FDGE::Binding::Papyrus {
    class Regex : public ScriptObject {
        ScriptType(Regex);

    public:
        Regex() noexcept = default;

        inline Regex(std::string_view pattern, std::regex::flag_type flags)
                : _pattern(pattern.data()), _regex(pattern.data(), pattern.size(), flags) {
        }

        [[nodiscard]] inline std::regex::flag_type GetFlags() const noexcept {
            return _regex.flags();
        }

        [[nodiscard]] inline std::string_view GetPattern() const noexcept {
            return _pattern;
        }

        [[nodiscard]] inline const std::regex& GetCompiled() const noexcept {
            return _regex;
        }

        [[nodiscard]] RE::BSFixedString ToString() const noexcept override;

        [[nodiscard]] bool Equals(const ScriptObject* other) const noexcept override;

        [[nodiscard]] std::size_t GetHashCode() const noexcept;

    private:
        articuno_serialize(ar) {
            ar <=> articuno::kv(_pattern, "pattern");
            auto flags = static_cast<uint32_t>(_regex.flags());
            ar <=> articuno::kv(flags, "flags");
        }

        articuno_deserialize(ar) {
            ar <=> articuno::kv(_pattern, "pattern");
            uint32_t flags;
            ar <=> articuno::kv(flags, "flags");
            _regex = std::regex(_pattern, static_cast<std::regex::flag_type>(flags));
        }

        std::regex _regex;
        std::string _pattern;

        friend class articuno::access;
    };

    class MatchResult : public ScriptObject {
        ScriptType(MatchResult);

    public:
        MatchResult() noexcept = default;

        inline MatchResult(std::string&& value, int32_t startIndex) noexcept
                : _value(std::move(value)), _startIndex(startIndex) {
        }

        [[nodiscard]] inline std::string_view GetValue() const noexcept {
            return _value;
        }

        [[nodiscard]] inline int32_t GetStartIndex() const noexcept {
            return _startIndex;
        }

        [[nodiscard]] RE::BSFixedString ToString() const noexcept override;

        [[nodiscard]] bool Equals(const ScriptObject* other) const noexcept override;

        [[nodiscard]] std::size_t GetHashCode() const noexcept;

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_startIndex, "startIndex");
        }

        std::string _value;
        std::int32_t _startIndex{0};

        friend class articuno::access;
    };
}
