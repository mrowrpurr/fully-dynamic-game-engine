#pragma once

#include "Any.h"

namespace FDGE::Binding::Papyrus {
    class PriorityQueue : public ScriptObject {
    ScriptType(PriorityQueue)

    public:
        [[nodiscard]] int32_t GetSize() const noexcept;

        void Enqueue(int32_t priority, Any* value);

        Any* Dequeue() noexcept;

        Any* Peek() noexcept;

    private:
        struct Node {
            inline Node() noexcept = default;

            inline Node(int32_t priority, Any* value) noexcept
                : Priority(priority), Value(value) {
            }

            int32_t Priority{0};
            ScriptObjectHandle<Any> Value;

            articuno_serde(ar) {
                ar <=> articuno::kv(Priority, "priority");
                ar <=> articuno::kv(Value, "value");
            }

            [[nodiscard]] inline auto operator<=>(const Node& other) const noexcept {
                return Priority <=> other.Priority;
            }
        };

        articuno_serde(ar) {
            ar <=> articuno::self(_value);
        }

        mutable std::mutex _lock;
        std::priority_queue<Node> _value;

        friend class articuno::access;
    };
}
