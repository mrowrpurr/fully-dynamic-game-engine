#pragma once

#include "SequenceCollection.h"

namespace FDGE::Binding::Papyrus {
    class RandomAccessCollection : public SequenceCollection {
    ScriptType(RandomAccessCollection)

    public:
        [[nodiscard]] virtual ScriptObjectHandle<Any> GetValue(std::int32_t index) noexcept = 0;

        [[nodiscard]] virtual bool SetValue(std::int32_t index, Any* value) noexcept = 0;

        [[nodiscard]] virtual Any* Erase(int32_t index) noexcept = 0;

        [[nodiscard]] virtual int32_t ReverseIndexOf(Any* value) const noexcept = 0;

        [[nodiscard]] virtual ValueIterator* GetReverseValueIterator() noexcept = 0;

        [[nodiscard]] virtual ValueIterator* GetValueIteratorFrom(int32_t index) noexcept = 0;

        [[nodiscard]] virtual ValueIterator* GetReverseValueIteratorFrom(int32_t index) noexcept = 0;

    protected:
        inline RandomAccessCollection() noexcept = default;

    private:
        articuno_serde() {}

        friend class articuno::access;
    };
}
