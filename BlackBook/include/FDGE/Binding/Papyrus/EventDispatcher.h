#pragma once

#include <gluino/mix.h>
#include <FDGE/Logger.h>
#include <FDGE/Skyrim/SerDe.h>
#include <parallel_hashmap/phmap.h>
#include <RE/Skyrim.h>
#include <SKSE/SKSE.h>

#include "../../System.h"
#include "../../Hook/Serialization.h"

#undef GetObject

namespace FDGE::Binding::Papyrus {
    class PapyrusEventDispatcher {
    public:
        using object_ref = std::pair<RE::VMHandle, RE::BSFixedString>;

        virtual ~PapyrusEventDispatcher() noexcept = default;

        [[nodiscard]] inline std::string_view GetEventName() const noexcept {
            return _eventName;
        }

        static void ClearRegistrations(RE::VMHandle handle, RE::BSFixedString className) {
            auto ref = std::make_pair(handle, className);
            _handleRegistrations.erase_if({handle, className}, [&](auto& registrations) {
                for (auto& dispatcher : registrations) {
                    dispatcher->Unregister(ref);
                }
                return true;
            });
        }

    protected:
        struct hash_object_ref {
            std::size_t operator()(const object_ref& value) const noexcept {
                auto seed = gluino::str_hash<false>{}(value.second.c_str());
                seed ^= value.first + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                return seed;
            }
        };

        virtual void Unregister(const object_ref& object) noexcept = 0;

        inline explicit PapyrusEventDispatcher(std::string_view eventName) : _eventName(eventName.data()) {
            std::string eventFunctionName{"On"};
            eventFunctionName += eventName;
            _eventFunctionName = RE::BSFixedString(eventFunctionName.c_str());
        }

        [[nodiscard]] inline RE::BSFixedString GetEventFunctionName() const noexcept {
            return _eventFunctionName;
        }

        static inline Util::parallel_flat_hash_map<std::string_view, PapyrusEventDispatcher*> _dispatchers;
        static inline Util::parallel_flat_hash_map<object_ref,
            phmap::flat_hash_set<PapyrusEventDispatcher*>, 4, std::mutex, hash_object_ref> _handleRegistrations;

    private:
        std::string _eventName;
        RE::BSFixedString _eventFunctionName;
    };

    template <class... Args>
    class SelectivePapyrusEventDispatcher : public PapyrusEventDispatcher {
    private:
        class Callback : public RE::BSScript::IStackCallbackFunctor {
        public:
            void operator()(RE::BSScript::Variable) override {
            }

            void SetObject(const RE::BSTSmartPointer<RE::BSScript::Object>&) override {
            }
        };

    public:
        inline explicit SelectivePapyrusEventDispatcher(std::string_view eventName,
                                                        std::string_view registrationFunctionsPapyrusClass = "")
                : PapyrusEventDispatcher(eventName) {
            if (!_dispatchers.try_emplace(eventName, this).second) {
                throw std::invalid_argument(std::format("Conflicting Papyrus event name '{}'.", eventName));
            }
            if (!registrationFunctionsPapyrusClass.empty()) {
                RegisterRegistrationFunctions(registrationFunctionsPapyrusClass);
            }

            std::string serdeName("SelectivePapyrusEventDispatcher::");
            serdeName += eventName.data();
            Hook::RegisterSaveHandler(serdeName, [this](std::ostream& out) { OnGameSaved(out); });
            Hook::RegisterLoadHandler(serdeName, [this](std::istream& in) { OnGameLoaded(in); });
            _registration = System().Listen([this](const NewGameEvent&) { OnNewGame(); });
        }

        ~SelectivePapyrusEventDispatcher() override {
            _dispatchers.erase(GetEventName());
            std::string serdeName("SelectivePapyrusEventDispatcher::");
            serdeName += GetEventName().data();
            Hook::UnregisterSaveHandler(serdeName);
            Hook::UnregisterLoadHandler(serdeName);
        }

        template <class... PassedArgs>
        void SendEvent(PassedArgs&& ... args) const {
            if (_receivers.empty()) {
                return;
            }
            RE::FunctionArguments<Args...> functionArgs(std::forward<Args>(static_cast<Args>(args))...);
            auto vm = RE::SkyrimVM::GetSingleton()->impl;
            RE::BSFixedString fn(GetEventFunctionName());
            std::unique_lock lock(_lock);
            for (auto ref : _receivers) {
                RE::BSTSmartPointer<RE::BSScript::IStackCallbackFunctor> callback{new Callback()};
                vm->DispatchMethodCall(ref.first, ref.second, fn, &functionArgs, callback);
            }
        }

        template <class... PassedArgs>
        inline void operator()(PassedArgs&& ... args) const {
            SendEvent(args...);
        }

        bool RegisterListener(object_ref object) {
            if (!object.first) {
                Logger::Warn("Attempt to register for event '{}' with null handle.", GetEventName());
                return false;
            }
            std::unique_lock lock(_lock);
            return _receivers.emplace(object).second;
        }

        bool RegisterListener(RE::VMStackID callingStackID) {
            return RegisterListener(FindCallingObject(callingStackID));
        }

        bool UnregisterListener(object_ref object) {
            if (!object.first) {
                Logger::Warn("Attempt to unregister for event '{}' with null handle.", GetEventName());
                return false;
            }
            std::unique_lock lock(_lock);
            return _receivers.erase(object);
        }

        [[maybe_unused]] bool UnregisterListener(RE::VMStackID callingStackID) {
            return UnregisterListener(FindCallingObject(callingStackID));
        }

        void RegisterRegistrationFunctions(std::string_view papyrusClassName) {
            System().AfterDataLoaded([=]() {
                auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
                std::string name{"RegisterFor"};
                name += GetEventName();
                vm->RegisterFunction(name, papyrusClassName, HandleRegistration);

                name = "UnregisterFor";
                name += GetEventName();
                vm->RegisterFunction(name, papyrusClassName, HandleUnregistration);

                return true;
            });
        }

    protected:
        void Unregister(const object_ref& object) noexcept override {
            _receivers.erase(object);
        }

        void OnNewGame() {
            std::unique_lock lock(_lock);
            _receivers.clear();
        }

        void OnGameSaved(std::ostream& out) {
            if (!_receivers.empty()) {
                std::unique_lock lock(_lock);
                articuno::ryml::yaml_sink snk(out);
                snk << _receivers;
            }
        }

        void OnGameLoaded(std::istream& in) {
            std::unique_lock lock(_lock);
            _receivers.clear();
            articuno::ryml::yaml_source src(in);
            src >> _receivers;
        }

    private:
        static object_ref FindCallingObject(RE::VMStackID stackID) {
            auto vm = reinterpret_cast<RE::BSScript::Internal::VirtualMachine*>(
                    RE::SkyrimVM::GetSingleton()->impl.get());
            RE::BSSpinLockGuard lock(vm->runningStacksLock);
            auto result = vm->allRunningStacks.find(stackID);
            if (result == vm->allRunningStacks.end()) {
                return {};
            }
            auto* callingFrame = result->second->top->previousFrame;
            if (!callingFrame) {
                return {};
            }
            auto& self = callingFrame->self;
            if (!self.IsObject()) {
                return {};
            }
            return {self.GetObject()->GetHandle(), self.GetObject()->GetTypeInfo()->GetName()};
        }

        static std::pair<std::string, object_ref> GetRegistrationEventName(RE::VMStackID callingStackID,
                                                                           std::string_view prefix) {
            auto vm = reinterpret_cast<RE::BSScript::Internal::VirtualMachine*>(RE::SkyrimVM::GetSingleton()->impl.get());
            RE::BSSpinLockGuard lock(vm->runningStacksLock);
            auto result = vm->allRunningStacks.find(callingStackID);
            if (result == vm->allRunningStacks.end()) {
                return {};
            }
            auto* stackFrame = result->second->top;
            if (!stackFrame->owningFunction) {
                return {};
            }
            std::string_view eventName = stackFrame->owningFunction->GetName().c_str();
            if (!eventName.starts_with(prefix)) {
                return {};
            }
            eventName = eventName.substr(prefix.size());

            auto* callingFrame = result->second->top->previousFrame;
            if (!callingFrame) {
                return {};
            }
            auto& self = callingFrame->self;
            if (!self.IsObject()) {
                return {};
            }

            return {eventName.data(), {self.GetObject()->GetHandle(), self.GetObject()->GetTypeInfo()->GetName()}};
        }

        static void HandleRegistration(RE::BSScript::IVirtualMachine*, RE::VMStackID stackID, RE::StaticFunctionTag*) {
            auto eventData{GetRegistrationEventName(stackID, "RegisterFor")};
            _dispatchers.if_contains(eventData.first, [&](PapyrusEventDispatcher* dispatcher) {
                dynamic_cast<SelectivePapyrusEventDispatcher<Args...>*>(dispatcher)->RegisterListener(
                        eventData.second);
            });
        }

        static void HandleUnregistration(RE::BSScript::IVirtualMachine*, RE::VMStackID stackID,
                                         RE::StaticFunctionTag*) {
            auto eventData{GetRegistrationEventName(stackID, "UnregisterFor")};
            _dispatchers.if_contains(eventData.first, [&](PapyrusEventDispatcher* dispatcher) {
                dynamic_cast<SelectivePapyrusEventDispatcher<Args...>*>(dispatcher)->UnregisterListener(
                        eventData.second);
            });
        }

        mutable std::mutex _lock;
        mutable phmap::flat_hash_set<object_ref, hash_object_ref> _receivers;
        [[maybe_unused]] Util::EventRegistration _registration;
    };

    template <class... Args>
    class GlobalPapyrusEventDispatcher : public PapyrusEventDispatcher {
    public:
        inline explicit GlobalPapyrusEventDispatcher(std::string_view eventName)
                : PapyrusEventDispatcher(eventName) {
        }

        template <class... PassedArgs>
        inline void SendEvent(PassedArgs&& ... args) const {
            RE::FunctionArguments<Args...> functionArgs(std::forward<Args>(args)...);
            auto vm = RE::SkyrimVM::GetSingleton()->impl;
            vm->SendEventAll(GetEventFunctionName(), &functionArgs);
        }
    };
}