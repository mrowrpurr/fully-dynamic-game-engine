#pragma once

#include "MapCollection.h"

namespace FDGE::Binding::Papyrus {
    class Proxy : public ScriptObject {
    ScriptType(Proxy)
    public:
        [[nodiscard]] virtual MapCollection* Get();

        bool RegisterForProxyUpdating(RE::VMHandle handle);

        bool UnregisterForProxyUpdating(RE::VMHandle handle);

        bool RegisterForProxyUpdated(RE::VMHandle handle);

        bool UnregisterForProxyUpdated(RE::VMHandle handle);

    protected:
        inline Proxy() noexcept = default;

        void OnUpdating();

        void OnUpdated();

    private:
        articuno_serde() {}

        std::recursive_mutex _lock;
        phmap::flat_hash_set<RE::VMHandle> _updatingHandles;
        phmap::flat_hash_set<RE::VMHandle> _updatedHandles;

        friend class articuno::access;
    };
}
