#pragma once

#include <pcg_random.hpp>
#include <gluino/random_engine.h>

namespace FDGE::Util {
    /**
     * Provides a random engine suitable for default shared used.
     *
     * <p>
     * This engine uses the PCG algorithm for excellent statistical properties and performance. It is thread-safe and
     * supports multiple streams, allowing for different consumers to change the stream number to avoid any association
     * with other consumers. If multiple stream functionality is not needed then <code>FastRandomEngine</code> is an
     * option with better performance.
     * </p>
     *
     * <p>
     * The thread-safety imposes some overhead. If you can guarantee that your own use of a random engine does not
     * require thread safety in itself, and if you are not frequently creating engines, then creating your own for
     * single-threaded use will result in better performance. However the seeding of these engines is expensive and so
     * these shared engines are provided for cases where you would use a short-lived engine.
     * </p>
     */
    __declspec(dllexport) gluino::synchronized_random_engine<pcg32>& GetDefaultRandomEngine() noexcept;

    /**
     * Provides a random engine suitable for the fastest results.
     *
     * <p>
     * Unlike the <code>DefaultRandomEngine</code>, this one lacks support for multiple streams. However, it still has
     * excellent randomness and normal consumers will not notice any correlation with other consumers, and the
     * performance will be better. This engine is also thread-safe.
     * </p>
     *
     * <p>
     * The thread-safety imposes some overhead. If you can guarantee that your own use of a random engine does not
     * require thread safety in itself, and if you are not frequently creating engines, then creating your own for
     * single-threaded use will result in better performance. However the seeding of these engines is expensive and so
     * these shared engines are provided for cases where you would use a short-lived engine.
     * </p>
     */
    __declspec(dllexport) gluino::synchronized_random_engine<pcg32_fast>& GetFastRandomEngine() noexcept;
}
