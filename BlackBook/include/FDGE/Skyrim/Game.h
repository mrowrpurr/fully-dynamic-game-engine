#pragma once

#include <FDGE/Util/EventSource.h>

#include "CellsLoadingEvent.h"
#include "ReferenceLoadedEvent.h"

namespace FDGE::Skyrim {
    namespace detail {
        class GameManager;

        class Game : public Util::EventSource<CellsLoadingEvent>,
                     public Util::EventSource<ReferenceLoadedEvent> {
        public:
            using EventSource<CellsLoadingEvent>::Listen;
            using EventSource<CellsLoadingEvent>::ListenForever;
            using EventSource<CellsLoadingEvent>::StopListening;
            using EventSource<ReferenceLoadedEvent>::Listen;
            using EventSource<ReferenceLoadedEvent>::ListenForever;
            using EventSource<ReferenceLoadedEvent>::StopListening;

            Game(const Game&) = delete;

            Game& operator=(const Game&) = delete;

        protected:
            using EventSource<CellsLoadingEvent>::Emit;
            using EventSource<ReferenceLoadedEvent>::Emit;

        private:
            Game() = default;

            friend class GameManager;
        };
    }

    extern const __declspec(dllexport) detail::Game Game;
}