#pragma once

#include <shared_mutex>

#include <articuno/types/auto.h>
#include <FDGE/Hook/SerializationHook.h>
#include <FDGE/Skyrim/PortableID.h>
#include <FDGE/Skyrim/RTTI.h>
#include <FDGE/Util/Maps.h>
#include <gluino/concepts.h>
#include <gluino/extension.h>

#include "FormProperty.h"

namespace FDGE::Skyrim {
    class FormPropertyCollection : protected FDGE::Hook::SerializationHook {
    public:
        explicit FormPropertyCollection(std::string_view name);

        FormPropertyCollection(const FormPropertyCollection&) = delete;

        [[nodiscard]] std::optional<std::shared_ptr<FormProperty>> Get(
                const PortableID& form, std::string_view key) const noexcept;

        [[nodiscard]] inline std::optional<std::shared_ptr<FormProperty>> Get(
                const PortableID&& form, std::string_view key) const noexcept {
            return Get(form, key);
        }

        template <class T>
        requires (gluino::arithmetic<T> || FormPointer<T> || std::is_same_v<T, nullptr_t>)
        [[nodiscard]] std::pair<std::shared_ptr<FormProperty>, bool> Emplace(
                const PortableID& form, std::string_view key, T&& value, FormPropertyFlags flags = {}) {
            bool outer = false;
            bool inner = false;
            std::shared_ptr<FormProperty> result;
            for (; _properties.try_emplace_l(
                    form,
                    [&](Util::istr_parallel_flat_hash_map<std::string, std::shared_ptr<FormProperty>, 0>& map) {
                        for (; map.try_emplace_l(
                                key, [&](std::shared_ptr<FormProperty>& value) {
                                    if (!value) {
                                        value.reset(CreateFormProperty(key, std::forward<T>(value), flags));
                                    }
                                    result = value;
                                }); inner = true) {
                        }
                    }); outer = true) {
            }
            return {result, outer && inner};
        }

        template <class T>
        requires (gluino::arithmetic<T> || FormPointer<T> || std::is_same_v<T, nullptr_t>)
        [[nodiscard]] inline std::pair<std::shared_ptr<FormProperty>, bool> Emplace(
                const PortableID&& form, std::string_view key, T&& value, FormPropertyFlags flags = {}) {
            return Emplace(form, key, std::forward<T>(value), flags);
        }

        template <class T>
        requires (gluino::arithmetic<T> || FormPointer<T> || std::is_same_v<T, nullptr_t>)
        [[nodiscard]] std::shared_ptr<FormProperty> Put(
                const PortableID& form, std::string_view key, T&& value, FormPropertyFlags flags = {}) {
            while (_properties.try_emplace_l(
                    form,
                    [&](Util::istr_parallel_flat_hash_map<std::string, std::shared_ptr<FormProperty>, 0>& map) {
                        map.emplace(key).first->second.reset(new FormProperty(key, std::forward<T>(value), flags));
                    })) {
            }
        }

        template <class T>
        requires (gluino::arithmetic<T> || FormPointer<T> || std::is_same_v<T, nullptr_t>)
        [[nodiscard]] inline std::shared_ptr<FormProperty> Put(
                const PortableID&& form, std::string_view key, T&& value, FormPropertyFlags flags = {}) {
            return Put(form, key, std::forward<T>(value), flags);
        }

        inline void Clear() {
            _properties.clear();
        }

        void Clear(const PortableID& form);

        inline void Clear(const PortableID&& form) {
            Clear(form);
        }

        void Erase(const PortableID& form, std::string_view key);

        inline void Erase(const PortableID&& form, std::string_view key) {
            Erase(form, key);
        }

        [[nodiscard]] inline std::string_view GetName() const noexcept {
            return _name;
        }

    protected:
        void OnNewGame() override;

        void OnGameSaved(std::ostream& out) override;

        void OnGameLoaded(std::istream& in) override;

    private:
        articuno_serde(ar) {
            ar <=> articuno::self(_properties);
        }

        template <class T>
        [[nodiscard]] static inline FormProperty* CreateFormProperty(std::string_view key, T&& value,
                                                                     FormPropertyFlags flags = {}) {
            return new FormProperty(key, std::forward<T>(value), flags);
        }

        std::string _name;
        Util::parallel_flat_hash_map<PortableID,
                Util::istr_parallel_flat_hash_map<std::string, std::shared_ptr<FormProperty>, 0>> _properties;

        friend class articuno::access;
    };

    namespace detail::FormPropertyCollection {
        struct GetProperty {
            template <FormPointer T>
            [[nodiscard]] inline std::optional<std::shared_ptr<FormProperty>> operator()(
                    const T* form, const class FormPropertyCollection& collection,
                    std::string_view key) const noexcept {
                return collection.Get(form, key);
            }

            template <FormPointer T>
            [[nodiscard]] inline std::optional<std::shared_ptr<FormProperty>> operator()(
                    const PortableID& formID, const class FormPropertyCollection& collection,
                    std::string_view key) const noexcept {
                return collection.Get(formID, key);
            }
        };

        struct SetProperty {
            template <Form F, class T>
            [[nodiscard]] inline std::shared_ptr<FormProperty> operator()(
                    const F* form, class FormPropertyCollection& collection, std::string_view key, T&& value,
                    FormPropertyFlags flags = {}) const {
                return collection.Put(form, key, std::forward<T>(value), flags);
            }

            template <class T>
            [[nodiscard]] inline std::shared_ptr<FormProperty> operator()(
                    const PortableID& formID, class FormPropertyCollection& collection, std::string_view key, T&& value,
                    FormPropertyFlags flags = {}) const {
                return collection.Put(formID, key, std::forward<T>(value), flags);
            }
        };

        struct EmplaceProperty {
            template <Form F, class T>
            [[nodiscard]] inline std::pair<std::shared_ptr<FormProperty>, bool> operator()(
                    const F* form, class FormPropertyCollection& collection, std::string_view key, T&& value,
                    FormPropertyFlags flags = {}) const {
                return collection.Emplace(form, key, std::forward<T>(value), flags);
            }

            template <class T>
            [[nodiscard]] inline std::pair<std::shared_ptr<FormProperty>, bool> operator()(
                    const PortableID& formID, class FormPropertyCollection& collection, std::string_view key, T&& value,
                    FormPropertyFlags flags = {}) const {
                return collection.Emplace(formID, key, std::forward<T>(value), flags);
            }
        };

        struct EraseProperty {
            template <Form F, class T>
            inline void operator()(const F* form, class FormPropertyCollection& collection,
                                   std::string_view key) const {
                collection.Erase(form, key);
            }

            template <class T>
            inline void operator()(const PortableID& formID, class FormPropertyCollection& collection,
                                   std::string_view key) const {
                collection.Erase(formID, key);
            }
        };

        struct ClearProperties {
            template <Form F, class T>
            inline void operator()(const F* form, class FormPropertyCollection& collection) const {
                collection.Clear(form);
            }

            template <class T>
            inline void operator()(const PortableID& formID, class FormPropertyCollection& collection) const {
                collection.Clear(formID);
            }
        };
    }

    [[maybe_unused]] constexpr gluino::extension<detail::FormPropertyCollection::GetProperty> GetProperty{};
    [[maybe_unused]] constexpr gluino::extension<detail::FormPropertyCollection::SetProperty> SetProperty{};
    [[maybe_unused]] constexpr gluino::extension<detail::FormPropertyCollection::EmplaceProperty> EmplaceProperty{};
    [[maybe_unused]] constexpr gluino::extension<detail::FormPropertyCollection::EraseProperty> EraseProperty{};
    [[maybe_unused]] constexpr gluino::extension<detail::FormPropertyCollection::ClearProperties> ClearProperties{};
}
