#include "FDGEConfig.h"

#include <FDGE/Config/DynamicFileProxy.h>
#include <FDGE/Config/FileProxy.h>

using namespace FDGE;
using namespace FDGE::Config;

Proxy<FDGEConfig>& FDGEConfig::GetProxy() noexcept {
    static DynamicFileProxy<FDGEConfig> proxy("Data\\SKSE\\Plugins\\!Fully Dynamic Game Engine");
    Proxy<FDGEConfig>& p = proxy;
    static std::atomic_bool initialized;
    if (!initialized.exchange(true)) {
        p.Refresh();
    }
    return p;
}
