#pragma once

#include <FDGE/Config/PluginConfig.h>
#include <FDGE/Config/Proxy.h>

namespace FDGE {
    gluino_enum(CosaveFormat, uint32_t,
        (Auto, 0),
        (YAML, 1),
        (YAMLWithZlib, 2),
        (YAMLWithZstd, 3));

    class Cosave {
    public:
        [[nodiscard]] inline CosaveFormat GetFormat() const noexcept {
            return _format;
        }

        inline void SetFormat(CosaveFormat format) noexcept {
            _format = format;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_format, "format");
        }

        CosaveFormat _format{CosaveFormat::Auto};

        friend class articuno::access;
    };

    class Fixes {
    public:
        [[nodiscard]] inline bool IsRemoteDesktopEnabled() const noexcept {
            return _remoteDesktopEnabled;
        }

        inline void SetRemoteDesktopEnabled(bool remoteDesktopEnabled) noexcept {
            _remoteDesktopEnabled = remoteDesktopEnabled;
        }

        [[nodiscard]] inline bool IsZeroWeightProjectilesEnabled() const noexcept {
            return _zeroWeightProjectilesEnabled;
        }

        inline void SetZeroWeightProjectilesEnabled(bool zeroWeightProjectilesEnabled) noexcept {
            _zeroWeightProjectilesEnabled = zeroWeightProjectilesEnabled;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_remoteDesktopEnabled, "enableRemoteDesktop");
            ar <=> articuno::kv(_zeroWeightProjectilesEnabled, "zeroWeightProjectiles");
        }

        bool _remoteDesktopEnabled{true};
        bool _zeroWeightProjectilesEnabled{true};

        friend class articuno::access;
    };

    class Modules {
    public:
        [[nodiscard]] inline bool IsDymodsEnabled() const noexcept {
            return _enableDymods;
        }

        inline void SetDymodsEnabled(bool dymodsEnabled) noexcept {
            _enableDymods = dymodsEnabled;
        }

        [[nodiscard]] inline bool IsScriptObjectsEnabled() const noexcept {
            return _enableScriptObjects;
        }

        inline void SetScriptObjectsEnabled(bool scriptObjectsEnabled) noexcept {
            _enableScriptObjects = scriptObjectsEnabled;
        }

        [[nodiscard]] inline bool IsConsoleCommandsEnabled() const noexcept {
            return _enableConsoleCommands;
        }

        inline void SetConsoleCommandsEnabled(bool consoleCommandsEnabled) noexcept {
            _enableConsoleCommands = consoleCommandsEnabled;
        }

        [[nodiscard]] inline bool IsSerializationEnabled() const noexcept {
            return _enableSerialization;
        }

        inline void SetSerializationEnabled(bool serializationEnabled) noexcept {
            _enableSerialization = serializationEnabled;
        }

        [[nodiscard]] inline bool IsEditorIDPreservationEnabled() const noexcept {
            return _enableEditorIDPreservation;
        }

        inline void SetEditorIDPreservationEnabled(bool editorIDPreservationEnabled) noexcept {
            _enableEditorIDPreservation = editorIDPreservationEnabled;
        }

        [[nodiscard]] inline bool IsReverseLookupDatabaseEnabled() const noexcept {
            return _enableReverseLookupDatabase;
        }

        inline void SetReverseLookupDatabaseEnabled(bool reverseLookupDatabaseEnabled) noexcept {
            _enableReverseLookupDatabase = reverseLookupDatabaseEnabled;
        }

        [[nodiscard]] inline bool IsAddressLibraryDumpEnabled() const noexcept {
            return _enableAddressLibraryDump;
        }

        inline void SetAddressLibraryDumpEnabled(bool addressLibraryDumpEnabled) noexcept {
            _enableAddressLibraryDump = addressLibraryDumpEnabled;
        }

        [[nodiscard]] inline bool IsFixesEnabled() const noexcept {
            return _enableFixes;
        }

        inline void SetFixesEnabled(bool fixesEnabled) noexcept {
            _enableFixes = fixesEnabled;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_enableDymods, "enableDymods");
            ar <=> articuno::kv(_enableScriptObjects, "enableScriptObjects");
            ar <=> articuno::kv(_enableConsoleCommands, "enableConsoleCommands");
            ar <=> articuno::kv(_enableSerialization, "enableSerialization");
            ar <=> articuno::kv(_enableReverseLookupDatabase, "enableEditorIDPreservation");
            ar <=> articuno::kv(_enableReverseLookupDatabase, "enableReverseLookupDatabase");
            ar <=> articuno::kv(_enableAddressLibraryDump, "enableAddressLibraryDump");
            ar <=> articuno::kv(_enableFixes, "enableFixes");
        }

        bool _enableDymods{true};
        bool _enableScriptObjects{true};
        bool _enableConsoleCommands{true};
        bool _enableSerialization{true};
        bool _enableEditorIDPreservation{true};
        bool _enableReverseLookupDatabase{true};
        bool _enableAddressLibraryDump{false};
        bool _enableFixes{true};

        friend class articuno::access;
    };

    class PapyrusTesting {
    public:
        [[nodiscard]] inline bool IsEnabled() const noexcept {
            return _enabled;
        }

        inline void SetEnabled(bool enabled) noexcept {
            _enabled = enabled;
        }

        [[nodiscard]] inline bool GetRunOnNewGame() const noexcept {
            return _runOnNewGame;
        }

        inline void SetRunOnNewGame(bool runOnNewGame) noexcept {
            _runOnNewGame = runOnNewGame;
        }

        [[nodiscard]] inline bool GetRunOnLoad() const noexcept {
            return _runOnLoad;
        }

        inline void SetRunOnLoad(bool runOnLoad) noexcept {
            _runOnLoad = runOnLoad;
        }

        [[nodiscard]] inline bool IsLoggingEnabled() const noexcept {
            return _logging;
        }

        inline void SetLoggingEnabled(bool loggingEnabled) noexcept {
            _logging = loggingEnabled;
        }

        [[nodiscard]] inline bool IsJUnitEnabled() const noexcept {
            return _junit;
        }

        inline void SetJUnitEnabled(bool junitEnabled) noexcept {
            _junit = junitEnabled;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_enabled, "enabled");
            ar <=> articuno::kv(_runOnNewGame, "runOnNewGame");
            ar <=> articuno::kv(_runOnLoad, "runOnLoad");
            ar <=> articuno::kv(_logging, "logging");
            ar <=> articuno::kv(_junit, "junit");
        }

        bool _enabled{true};
        bool _runOnNewGame{false};
        bool _runOnLoad{false};
        bool _logging{true};
        bool _junit{true};

        friend class articuno::access;
    };

    class DebugConfig : public Config::DebugConfig {
    public:
        [[nodiscard]] inline Modules& GetModules() noexcept {
            return _modules;
        }

        [[nodiscard]] inline const Modules& GetModules() const noexcept {
            return _modules;
        }

        [[nodiscard]] inline PapyrusTesting& GetPapyrusTesting() noexcept {
            return _papyrusTesting;
        }

        [[nodiscard]] inline const PapyrusTesting& GetPapyrusTesting() const noexcept {
            return _papyrusTesting;
        }

    private:
        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_modules, "modules");
            ar <=> articuno::kv(_papyrusTesting, "papyrusTesting");
            articuno_super(Config::DebugConfig, ar, flags);
        }

        Modules _modules;
        PapyrusTesting _papyrusTesting;

        friend class articuno::access;
    };

    class PerformanceConfig {
    public:
        [[nodiscard]] inline uint64_t GetEditorIDReservation() const noexcept {
            return _editorIDReservation;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_editorIDReservation, "editorIdReservation");
        }

        uint64_t _editorIDReservation{1048576};

        friend class articuno::access;
    };

    class FDGEConfig : public Config::PluginConfig<DebugConfig> {
    public:
        [[nodiscard]] inline PerformanceConfig& GetPerformance() noexcept {
            return _performance;
        }

        [[nodiscard]] inline const PerformanceConfig& GetPerformance() const noexcept {
            return _performance;
        }

        [[nodiscard]] inline Fixes& GetFixes() noexcept {
            return _fixes;
        }

        [[nodiscard]] inline const Fixes& GetFixes() const noexcept {
            return _fixes;
        }

        [[nodiscard]] inline Cosave& GetCosave() noexcept {
            return _cosave;
        }

        [[nodiscard]] inline const Cosave& GetCosave() const noexcept {
            return _cosave;
        }

        static Config::Proxy<FDGEConfig>& GetProxy() noexcept;

    private:
        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_performance, "performance");
            ar <=> articuno::kv(_fixes, "fixes");
            ar <=> articuno::kv(_cosave, "cosave");
            articuno_super(Config::PluginConfig<DebugConfig>, ar, flags);
        }

        PerformanceConfig _performance;
        Fixes _fixes;
        Cosave _cosave;

        friend class articuno::access;
    };
}

namespace articuno::serde {
    template <::articuno::serializing_archive Archive>
    void serde(Archive& ar, const FDGE::CosaveFormat& value, articuno::value_flags) {
        std::string_view str;
        switch (static_cast<FDGE::CosaveFormat::enum_type>(value)) {
            case FDGE::CosaveFormat::enum_type::Auto:
                str = "auto";
            case FDGE::CosaveFormat::enum_type::YAML:
                str = "yaml";
            case FDGE::CosaveFormat::enum_type::YAMLWithZstd:
                str = "yaml+zstd";
        }
        ar <=> articuno::self(str);
    }

    template <::articuno::deserializing_archive Archive>
    void serde(Archive& ar, FDGE::CosaveFormat& value, articuno::value_flags) {
        std::string str;
        gluino::str_equal_to<false> equals;
        if ((ar <=> ::articuno::self(str))) {
            if (equals(str, "auto")) {
                value = FDGE::CosaveFormat::Auto;
            } else if (equals(str, "yaml")) {
                value = FDGE::CosaveFormat::YAML;
            } else if (equals(str, "yaml+zlib")) {
                value = FDGE::CosaveFormat::YAMLWithZlib;
            } else if (equals(str, "yaml+zstd")) {
                value = FDGE::CosaveFormat::YAMLWithZstd;
            }
        } else {
            value = FDGE::CosaveFormat::Auto;
        }
    }
}
