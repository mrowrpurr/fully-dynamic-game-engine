#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>

#include "FDGEConfig.h"

using namespace FDGE;

SKSEPlugin(
    Name = "Fully Dynamic Game Engine";
    Version = {1, 0, 0};
    Author = "Charmed Baryon";
    Email = "charmedbaryon@elderscrolls-ng.com";
    SKSESerializationPluginID = "FDGE";
    UsesDeclarativeMessaging = false;
    PreInit = []() {
        Logger::Initialize(FDGEConfig::GetProxy());
        FDGEConfig::GetProxy().StartWatching();
    };
);
