#include <FDGE/Util/RandomEngine.h>

using namespace FDGE;
using namespace gluino;

gluino::synchronized_random_engine<pcg32>& Util::GetDefaultRandomEngine() noexcept {
    static gluino::synchronized_random_engine<pcg32> defaultRandomEngine;
    return defaultRandomEngine;
}

gluino::synchronized_random_engine<pcg32_fast>& Util::GetFastRandomEngine() noexcept {
    static gluino::synchronized_random_engine<pcg32_fast> fastRandomEngine;
    return fastRandomEngine;
}
