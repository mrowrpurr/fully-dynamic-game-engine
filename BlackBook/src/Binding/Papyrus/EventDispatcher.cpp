#include <FDGE/Binding/Papyrus/EventDispatcher.h>

#include <FDGE/Hook/FunctionHook.h>
#include <FDGE/Skyrim/SerDe.h>

using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Hook;
using namespace RE;
using namespace RE::BSScript;

namespace {
    OnSKSELoaded {
        static FunctionHook<int32_t __fastcall(Object*)> ReleaseObjectReference(
                104253, [](Object* self) {
                    if (!self) {
                        return ReleaseObjectReference(self);
                    }

                    auto handle = self->GetHandle();
                    BSFixedString name = self->GetTypeInfo()->GetName();
                    auto result = ReleaseObjectReference(self);
                    if (result <= 1) {
                        PapyrusEventDispatcher::ClearRegistrations(handle, name);
                    }
                    return result;
                });
    }
}
