#include <FDGE/Binding/Papyrus/DynamicArray.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

ScriptObjectHandle<Any> DynamicArray::GetValue(std::int32_t index) noexcept {
    std::unique_lock lock(_lock);
    if (index < 0) {
        index = GetSize() + index;
    }
    if (index < 0 || index >= GetSize()) {
        return {};
    }
    return _value[index];
}

bool DynamicArray::SetValue(std::int32_t index, Any* value) noexcept {
    std::unique_lock lock(_lock);
    if (index < 0) {
        index = GetSize() + index;
    }
    if (index < 0 || index >= GetSize()) {
        return false;
    }
    _value[index] = value;
    ++_version;
    return true;
}

void DynamicArray::Append(Any* value) noexcept {
    std::unique_lock lock(_lock);
    _value.emplace_back(value);
    ++_version;
}

Any* DynamicArray::PopBack() noexcept {
    std::unique_lock lock(_lock);
    if (GetSize()) {
        auto result = _value.back();
        _value.pop_back();
        ++_version;
        return result;
    }
    return nullptr;
}

Any* DynamicArray::Erase(int32_t index) noexcept {
    std::unique_lock lock(_lock);
    if (index < 0) {
        index = GetSize() + index;
    }
    if (index < 0 || index >= GetSize()) {
        return nullptr;
    }
    auto result = _value[index];
    _value.erase(_value.begin() + index);
    ++_version;
    return result;
}

int32_t DynamicArray::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

bool DynamicArray::Clear() noexcept {
    std::unique_lock lock(_lock);
    if (!_value.empty()) {
        _value.clear();
        ++_version;
        return true;
    }
    return false;
}

bool DynamicArray::Contains(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    for (auto& entry : _value) {
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                return true;
            }
        } else if (value->Equals(candidate)) {
            return true;
        }
    }
    return false;
}

int32_t DynamicArray::Count(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    int32_t result = 0;
    for (auto& entry : _value) {
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                ++result;
            }
        } else if (value->Equals(candidate)) {
            ++result;
        }
    }
    return result;
}

int32_t DynamicArray::IndexOf(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    int32_t index = 0;
    for (auto& entry : _value) {
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                return index;
            }
        } else if (value->Equals(candidate)) {
            return index;
        }
        ++index;
    }
    return -1;
}

int32_t DynamicArray::ReverseIndexOf(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    for (int32_t index = static_cast<int32_t>(_value.size()) - 1; index >= 0; --index) {
        auto& entry = _value[index];
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                return index;
            }
        } else if (value->Equals(candidate)) {
            return index;
        }
    }
    return -1;
}

ValueIterator* DynamicArray::GetValueIterator() noexcept {
    return new DynamicArrayIterator(this, false);
}

ValueIterator* DynamicArray::GetReverseValueIterator() noexcept {
    return new DynamicArrayIterator(this, true);
}

ValueIterator* DynamicArray::GetValueIteratorFrom(int32_t index) noexcept {
    return new DynamicArrayIterator(this, false, index);
}

ValueIterator* DynamicArray::GetReverseValueIteratorFrom(int32_t index) noexcept {
    return new DynamicArrayIterator(this, true, index);
}

void DynamicArray::Reserve(int32_t capacity) noexcept {
    std::unique_lock lock(_lock);
    if (capacity < 0) {
        Logger::Error("Cannot reserve DynamicArray capacity of less than zero.");
        return;
    }
    _value.reserve(capacity);
}

void DynamicArray::Resize(int32_t size) noexcept {
    std::unique_lock lock(_lock);
    if (size < 0) {
        Logger::Error("Cannot resize DynamicArray to a size of less than zero.");
        return;
    }
    _value.resize(size);
    ++_version;
}

DynamicArrayIterator::DynamicArrayIterator(ScriptObjectHandle<DynamicArray> parent, bool reverse) noexcept
    : _parent(std::move(parent)), _reverse(reverse) {
    DynamicArray* array = _parent;
    std::unique_lock lock(array->_lock);
    _version = array->_version;
    if (_reverse) {
        _index = array->GetSize() - 1;
    }
    if (IsValid()) {
        _value = array->GetValue(static_cast<int32_t>(_index));
    }
}

DynamicArrayIterator::DynamicArrayIterator(ScriptObjectHandle<DynamicArray> parent, bool reverse,
                                           int32_t index) noexcept : _parent(std::move(parent)), _reverse(reverse) {
    DynamicArray* array = _parent;
    std::unique_lock lock(array->_lock);
    if (index < 0) {
        index = array->GetSize() + index;
    }
    if (_reverse) {
        index = array->GetSize() - index - 1;
    }
    _index = index;
    _version = array->_version;
    if (IsValid()) {
        _value = array->GetValue(static_cast<int32_t>(_index));
    }
}

bool DynamicArrayIterator::IsValid() noexcept {
    DynamicArray* array = _parent;
    return array->GetVersion() == _version && _index < array->GetSize() && _index >= 0;
}

bool DynamicArrayIterator::HasMore() noexcept {
    DynamicArray* array = _parent;
    return _index < array->GetSize() && _index >= 0;
}

Any* DynamicArrayIterator::GetValue() noexcept {
    return _value.GetObject();
}

bool DynamicArrayIterator::SetValue(Any* value) noexcept {
    DynamicArray* array = _parent;
    std::unique_lock lock(array->_lock);
    if (!IsValid()) {
        return false;
    }
    _version = ++array->_version;
    array->_value[_index] = value;
    return true;
}

bool DynamicArrayIterator::Next() noexcept {
    DynamicArray* array = _parent;
    std::unique_lock lock(array->_lock);
    _reverse ? --_index : ++_index;
    if (!IsValid()) {
        _value = {};
        return false;
    }
    _value = array->GetValue(static_cast<int32_t>(_index));
    return true;
}

namespace {
    RegisterScriptType(DynamicArray);
    RegisterScriptType(DynamicArrayIterator);

    PapyrusClass(DynamicArray) {
        PapyrusStaticFunction(Create, int32_t initialCapacity) {
            auto* array = new DynamicArray();
            array->Reserve(initialCapacity);
            return array;
        };

        PapyrusFunction(__GetCapacity, DynamicArray* self) {
            if (!self) {
                return 0;
            }
            return self->GetCapacity();
        };

        PapyrusFunction(Append, DynamicArray* self, Any* value) {
            if (self) {
                self->Append(value);
            }
        };

        PapyrusFunction(PopBack, DynamicArray* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->PopBack();
        };

        PapyrusFunction(Reserve, DynamicArray* self, int32_t capacity) {
            if (self) {
                self->Reserve(capacity);
            }
        };

        PapyrusFunction(Resize, DynamicArray* self, int32_t size) {
            if (self) {
                self->Resize(size);
            }
        };
    };
}
