#include <FDGE/Binding/Papyrus/Assert.h>
#include <FDGE/Binding/Papyrus/PapyrusClass.h>
#include <FDGE/Binding/Papyrus/TestSuite.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Skyrim/SerDe.h>
#include <FDGE/System.h>

#include "../../FDGEConfig.h"

using namespace articuno;
using namespace articuno::ryml;
using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace gluino;
using namespace RE;
using namespace RE::BSScript;
using namespace RE::BSScript::Internal;

#undef GetClassName

namespace {
    std::filesystem::path TestResultsPath;

    gluino_autorun {
        TestResultsPath = "My Games";
        TestResultsPath /= "Skyrim Special Edition";
        TestResultsPath /= "Logs";
        TestResultsPath /= "Script";
    }

    class TestResult {
       public:
        inline TestResult() noexcept = default;

        // TODO: Handle AllAsserts
        TestResult(BSFixedString className, BSFixedString name, const std::chrono::duration<double_t>& time,
                   bool skipped = false, std::string error = "") noexcept
            : _className(className), _name(name), _error(std::move(error)), _time(time), _skipped(skipped) {
            if (name.size() > 5 && name[4] == '_') {
                std::string bddName = name.c_str();
                bddName = bddName.substr(5);
                for (auto& c : bddName) {
                    if (c == '_') {
                        c = ' ';
                    }
                }
                _name = bddName;
            }
        }

        [[nodiscard]] inline BSFixedString GetClassName() const noexcept { return _className; }

        [[nodiscard]] inline BSFixedString GetName() const noexcept { return _name; }

        [[nodiscard]] inline std::string_view GetError() const noexcept { return _error; }

        [[nodiscard]] inline const std::chrono::duration<double_t>& GetTime() const noexcept { return _time; }

        [[nodiscard]] inline bool IsSkipped() const noexcept { return _skipped; }

       private:
        articuno_serialize(ar) {
            ar <=> kv(_className, "classname", value_flags::attribute);
            ar <=> kv(_name, "name", value_flags::attribute);
            ar <=> kv(_time, "time", value_flags::attribute);
            if (_skipped) {
                // Error will be empty, resulting in empty tag.
                ar <=> kv(_error, "skipped");
            } else if (!_error.empty()) {
                ar <=> kv(_error, "failure");
            }
        }

        BSFixedString _className;
        BSFixedString _name;
        std::string _error;
        std::chrono::duration<double_t> _time;
        bool _skipped{false};

        friend class articuno::access;
    };

    class TestSuiteResult {
       public:
        [[nodiscard]] inline BSFixedString GetClassName() const noexcept { return _className; }

        inline void SetClassName(BSFixedString className) noexcept { _className = className; }

        [[nodiscard]] inline std::size_t GetFailed() const noexcept { return _failed; }

        [[nodiscard]] inline std::size_t GetSucceeded() const noexcept { return _succeeded; }

        [[nodiscard]] inline std::size_t GetSkipped() const noexcept { return _skipped; }

        [[nodiscard]] inline const std::chrono::duration<double_t>& GetTime() const noexcept { return _time; }

        inline void SetTime(const std::chrono::duration<double_t>& time) noexcept { _time = time; }

        [[nodiscard]] inline const std::chrono::time_point<std::chrono::system_clock>& GetStartTime() const noexcept {
            return _startTime;
        }

        inline void SetStartTime(const std::chrono::time_point<std::chrono::system_clock>& startTime) noexcept {
            _startTime = startTime;
        }

        [[nodiscard]] inline std::string_view GetHostName() const noexcept { return _hostName; }

        inline void SetHostName(std::string_view hostName) { _hostName = hostName.data(); }

        [[nodiscard]] inline const std::vector<TestResult>& GetTests() const noexcept { return _tests; }

        void AddTestResult(BSFixedString testName, const std::chrono::duration<double_t>& time,
                           std::string error = "") {
            if (error.empty()) {
                ++_succeeded;
            } else {
                ++_failed;
            }
            _tests.emplace_back(_className, testName, time, false, std::move(error));
        }

        void SkipTest(BSFixedString testName) {
            ++_skipped;
            _tests.emplace_back(_className, testName, 0s, true);
        }

       private:
        articuno_serialize(ar) {
            auto tests = _tests.size();
            ar <=> kv(_className, "name", value_flags::attribute);
            ar <=> kv(tests, "tests", value_flags::attribute);
            ar <=> kv(_failed, "failures", value_flags::attribute);
            ar <=> kv(_skipped, "skipped", value_flags::attribute);
            ar <=> kv(_time, "time", value_flags::attribute);
            ar <=> kv(_startTime, "timestamp", value_flags::attribute);
            ar <=> kv(_tests, "testcase");
        }

        BSFixedString _className;
        std::size_t _failed;
        std::size_t _succeeded;
        std::size_t _skipped;
        std::chrono::duration<double_t> _time;
        std::chrono::time_point<std::chrono::system_clock> _startTime;
        std::string _hostName;
        std::vector<TestResult> _tests;

        friend class articuno::access;
    };

    class TestSuiteRunner : public IStackCallbackFunctor {
       private:
        enum class StepType : uint8_t { PreSuite, PreTest, Test, PostTest, PostSuite, SkipTest };

        struct Step {
            Step(StepType type, const BSTSmartPointer<Object>& suite, BSFixedString functionName) noexcept
                : Type(type), Suite(suite), FunctionName(functionName) {}

            StepType Type;
            BSTSmartPointer<Object> Suite;
            BSFixedString FunctionName;
        };

       public:
        inline TestSuiteRunner() noexcept = default;

        explicit TestSuiteRunner(std::span<ScriptObjectHandle<TestSuite>> suites, bool allAsserts = false,
                                 std::string_view testGroupFilter = "", std::string_view suiteFilter = "",
                                 std::string_view testFilter = "", bool forceJUnit = false,
                                 bool terminateOnCompletion = false)
                : _allAsserts(allAsserts), _forceJUnit(forceJUnit), _terminateOnCompletion(terminateOnCompletion) {
            DWORD hostNameBufferSize;
            GetComputerNameA(_hostName.data(), &hostNameBufferSize);
            ++hostNameBufferSize;
            _hostName.resize(hostNameBufferSize);
            GetComputerNameA(_hostName.data(), &hostNameBufferSize);

            std::regex testGroupFilterPattern(testGroupFilter.data(), std::regex::optimize | std::regex::icase);
            std::regex suiteFilterPattern(suiteFilter.data(), std::regex::optimize | std::regex::icase);
            std::regex testFilterPattern(testFilter.data(), std::regex::optimize | std::regex::icase);

            for (auto& obj : suites) {
                auto* suite = obj.GetObject();
                if (!suite) {
                    Logger::Warn("None suite was provided to test suite runner.");
                    continue;
                }
                if (!suiteFilter.empty() && !std::regex_match(obj->GetTypeInfo()->GetName(), suiteFilterPattern)) {
                    Logger::Debug("Skipping test suite {}, suite does not match test suite filter '{}'.",
                                  obj->GetTypeInfo()->GetName(), suiteFilter);
                    continue;
                }
                _steps.emplace(StepType::PreSuite, obj, "OnPreTestSuite");
                for (auto* typeInfo = obj->GetTypeInfo();
                     typeInfo && !str_equal_to<false>{}(typeInfo->GetName(), "TestSuite");
                     typeInfo = typeInfo->GetParent()) {
                    auto funcCount = typeInfo->GetNumMemberFuncs();
                    for (std::size_t i = 0; i < funcCount; ++i) {
                        auto func = typeInfo->GetMemberFuncIter()[i].func;
                        if (func->GetStateName() != obj->currentState) {
                            continue;
                        }
                        auto funcName = func->GetName();
                        if (!testFilter.empty() && !std::regex_match(funcName.c_str(), testFilterPattern)) {
                            Logger::Debug("Skipping test {} in suite {}, test does not match test filter '{}'.",
                                          funcName.c_str(), obj->GetTypeInfo()->GetName(), testFilter);
                            continue;
                        }
                        std::string name = funcName.c_str();
                        std::transform(name.begin(), name.end(), name.begin(),
                                       [](char c) { return static_cast<char>(std::tolower(c)); });
                        if (name.starts_with("test")) {
                            if (func->GetParamCount()) {
                                Logger::Warn("Apparent test function '{}' does not match expected signature, "
                                    "function should take no arguments.", funcName.c_str());
                                continue;
                            }
                            if (!func->GetReturnType().IsNoneObject() && !func->GetReturnType().IsString()) {
                                Logger::Warn("Apparent test function '{}' does not match expected signature, "
                                    "function should return nothing or String.", funcName.c_str());
                                continue;
                            }
                            std::string docString = func->GetDocString().c_str();
                            std::transform(docString.begin(), docString.end(), docString.begin(), [](char c) {
                                return static_cast<char>(std::tolower(c));
                            });
                            if (docString.contains("@skip")) {
                                _steps.emplace(StepType::SkipTest, obj, funcName);
                                continue;
                            }
                            _steps.emplace(StepType::PreTest, obj, funcName);
                            _steps.emplace(StepType::Test, obj, funcName);
                            _steps.emplace(StepType::PostTest, obj, funcName);
                        }
                    }
                }
                _steps.emplace(StepType::PostSuite, obj, "OnPostTestSuite");
            }
        }

        void ReportResults() {
            FDGE_WIN_CHAR_TYPE myDocumentsPath[MAX_PATH];
            auto result = SHGetFolderPath(nullptr, CSIDL_PERSONAL, nullptr, SHGFP_TYPE_CURRENT, myDocumentsPath);
            if (result != S_OK) {
                Logger::Error("Unable to determine documents path.");
            }
            std::filesystem::path basePath(myDocumentsPath);
            std::filesystem::create_directories(basePath);
            basePath /= TestResultsPath;

            if (FDGEConfig::GetProxy()->GetDebug().GetPapyrusTesting().IsLoggingEnabled()) {
                std::filesystem::path logPath = basePath;
                logPath /= "PapyrusTests.3.log";
                if (std::filesystem::exists(logPath)) {
                    std::filesystem::remove(logPath);
                }
                for (int i = 2; i >= 0; --i) {
                    logPath = basePath;
                    logPath /= std::format("PapyrusTests.{}.log", i);
                    if (std::filesystem::exists(logPath)) {
                        std::filesystem::path newPath = basePath;
                        newPath /= std::format("PapyrusTests.{}.log", i + 1);
                        std::filesystem::copy_file(logPath, newPath);
                        std::filesystem::remove(logPath);
                    }
                }

                logPath = basePath;
                logPath /= "PapyrusTests.0.log";
                _logFile.open(logPath);
            }

            //                if (FDGEConfig::GetProxy()->GetDebug().GetPapyrusTesting().IsJUnitEnabled() || forceJUnit) {
            //                    // TODO: Enable with Articuno fixes, and convert to true XML Junit.
            //                    std::filesystem::path junitPath = basePath;
            //                    junitPath /= "TestSuiteRunner.yaml";
            //                    std::ofstream junit(junitPath);
            //                    if (junit.good()) {
            //                        yaml_sink out(junit);
            //                        //out << *this;
            //                    }
            //                }

            std::size_t successful{0};
            std::size_t failed{0};
            std::size_t skipped{0};
            for (auto& suiteResult : _results) {
                successful += suiteResult.GetSucceeded();
                failed += suiteResult.GetFailed();
                skipped += suiteResult.GetSkipped();
                Output("{} ({} successful, {} failed, {} skipped; ran at {} for {}):",
                       suiteResult.GetClassName().c_str(), suiteResult.GetSucceeded(), suiteResult.GetFailed(),
                       suiteResult.GetSkipped(), suiteResult.GetStartTime(), suiteResult.GetTime());
                for (auto& testResult : suiteResult.GetTests()) {
                    if (testResult.IsSkipped()) {
                        Output("\t{}: SKIPPED", testResult.GetName().c_str());
                    } else if (testResult.GetError().empty()) {
                        Output("\t{}: SUCCESS (Ran for {})", testResult.GetName().c_str(), testResult.GetTime());
                    } else {
                        Output("\t{}: FAILED (Ran for {})", testResult.GetName().c_str(), testResult.GetTime());
                        Output("\t\t{}", testResult.GetError());
                    }
                }
            }
            Output("Total: {} successful, {} failed, {} skipped", successful, failed, skipped);

            if (_terminateOnCompletion) {
                ExitProcess(S_OK);
            }
        }

        void operator()() {
            if (_steps.empty()) {
                ReportResults();
                return;
            }

            auto* vm = VirtualMachine::GetSingleton();
            BSTSmartPointer<IStackCallbackFunctor> self(this);
            IFunctionArguments* args = MakeFunctionArguments();
            BSFixedString functionName;
            switch (_steps.front().Type) {
                case StepType::PreSuite:
                    functionName = "OnPreTestSuite";
                    break;
                case StepType::PostSuite:
                    functionName = "OnPostTestSuite";
                    break;
                case StepType::PreTest:
                    functionName = "OnPreTest";
                    break;
                case StepType::PostTest:
                    functionName = "OnPostTest";
                    break;
                case StepType::Test:
                    functionName = _steps.front().FunctionName;
                    break;
                case StepType::SkipTest:
                    _results.back().SkipTest(_steps.front().FunctionName);
                    (*this)();
                    return;
            }
            vm->DispatchMethodCall(_steps.front().Suite, functionName, args, self);
        }

        void operator()(Variable result) override {
            switch (_steps.front().Type) {
                case StepType::PreSuite: {
                    auto& suite = _results.emplace_back();
                    suite.SetClassName(_steps.front().Suite->GetTypeInfo()->GetName());
                    suite.SetHostName(_hostName);
                    _suiteStart = std::chrono::system_clock::now();
                    suite.SetStartTime(_suiteStart);
                    break;
                }
                case StepType::PreTest: {
                    _testStart = std::chrono::system_clock::now();
                    break;
                }
                case StepType::Test: {
                    if (result.IsString()) {
                        _functionRename = result.GetString();
                    }
                    break;
                }
                case StepType::PostTest: {
                    auto time = std::chrono::system_clock::now() - _testStart;
                    std::vector<std::string> failures = Assert::FindAssertFailures(_steps.front().Suite->GetHandle());
                    std::string_view error = "";
                    if (!failures.empty()) {
                        error = failures[0];
                    }
                    _results.back().AddTestResult(_functionRename.empty() ? _steps.front().FunctionName :
                                                                          _functionRename,
                                                  std::chrono::duration_cast<std::chrono::duration<double_t>>(time),
                                                  error.data());
                    _functionRename = "";
                    break;
                }
                case StepType::PostSuite: {
                    auto time = std::chrono::system_clock::now() - _suiteStart;
                    _results.back().SetTime(std::chrono::duration_cast<std::chrono::duration<double_t>>(time));
                    break;
                }
                default:
                    break;
            }
            _steps.pop();
            (*this)();
        }

        void SetObject(const BSTSmartPointer<Object>&) override {}

       private:
        articuno_serialize(ar) { ar <=> kv(_results, "testsuite"); }

        template <class... FmtArgs>
        inline void Output(std::string_view fmt, FmtArgs... args) {
            std::string str = std::format(fmt, std::forward<FmtArgs>(args)...);
            FDGE::Console::Print(str);
            if (_logFile.good()) {
                _logFile << str << std::endl;
            }
        }

        std::ofstream _logFile;
        std::string _hostName;
        BSFixedString _functionRename;
        std::chrono::time_point<std::chrono::system_clock> _suiteStart;
        std::chrono::time_point<std::chrono::system_clock> _testStart;
        std::queue<Step> _steps;
        std::vector<TestSuiteResult> _results;
        bool _forceJUnit{false};
        bool _terminateOnCompletion{false};
        bool _allAsserts{false};

        friend class articuno::access;
    };

    RegisterScriptType(TestSuite)

        PapyrusClass(TestSuite) {
        PapyrusStaticFunction(RunAll, bool allAsserts) { return TestSuite::RunAll(allAsserts); };

        PapyrusFunction(Run, TestSuite* testSuite, bool allAsserts) {
            if (!testSuite) {
                return false;
            }
            return testSuite->Run(allAsserts);
        };
    }

    AfterSKSEPluginsLoaded {
        System().ListenForever([](const NewGameEvent&) {
            if (FDGEConfig::GetProxy()->GetDebug().GetPapyrusTesting().GetRunOnNewGame()) {
                TestSuite::RunAll(false, true);
            }
        });

        System().ListenForever([](const SaveGameLoadedEvent&) {
            if (FDGEConfig::GetProxy()->GetDebug().GetPapyrusTesting().GetRunOnLoad()) {
                TestSuite::RunAll(false, true);
            }
        });
    }
}  // namespace

bool TestSuite::RunAll(bool allAsserts, bool force, std::string_view testGroupPattern, std::string_view suitePattern,
                       std::string_view testPattern, bool forceJUnit, bool terminateOnCompletion) {
    if (!force && !FDGEConfig::GetProxy()->GetDebug().GetPapyrusTesting().IsEnabled()) {
        return false;
    }

    auto suites = DiscoverAutoTestSuites();

    std::vector<ScriptObjectHandle<TestSuite>> suiteInstances;
    for (auto& type : suites) {
        auto* suite = new TestSuite();
        suiteInstances.emplace_back(suite->Bind(type->GetName()));
    }

    auto* runner =
        new TestSuiteRunner(std::span<ScriptObjectHandle<TestSuite>>(suiteInstances.data(), suiteInstances.size()),
                            allAsserts, testGroupPattern, suitePattern, testPattern, forceJUnit, terminateOnCompletion);
    (*runner)();

    return true;
}

bool TestSuite::Run(bool allAsserts, bool force) {
    if (!force && !FDGEConfig::GetProxy()->GetDebug().GetPapyrusTesting().IsEnabled()) {
        return false;
    }

    ScriptObjectHandle<TestSuite> object = this;
    auto* runner = new TestSuiteRunner(std::span<ScriptObjectHandle<TestSuite>>(&object, 1), allAsserts);
    (*runner)();

    return true;
}

std::vector<BSTSmartPointer<ObjectTypeInfo>> TestSuite::DiscoverAutoTestSuites() {
    std::vector<BSTSmartPointer<ObjectTypeInfo>> results;
    results.reserve(256);

    auto* vm = VirtualMachine::GetSingleton();
    if (!vm) {
        return std::move(results);
    }

    static std::atomic_bool allScriptsLinked{false};
    static std::latch linkerLatch{1};
    if (!allScriptsLinked.exchange(true)) {
        std::filesystem::directory_iterator scriptsDir("Data\\Scripts");
        for (auto& file : scriptsDir) {
            if (!file.is_regular_file()) {
                continue;
            }
            auto scriptPath = file.path();
            auto extension = scriptPath.extension().string();
            if (str_equal_to<false>{}(extension, ".pex")) {
                BSFixedString name = scriptPath.stem().string().c_str();
                vm->linker.Process(name);
            }
        }
        linkerLatch.count_down();
    } else {
        linkerLatch.wait();
    }

    BSSpinLockGuard lock(vm->typeInfoLock);
    for (auto& entry : vm->objectTypeMap) {
        for (auto* typeInfo = entry.second->GetParent(); typeInfo != nullptr; typeInfo = typeInfo->GetParent()) {
            if (str_equal_to<false>{}(typeInfo->GetName(), "AutoTestSuite")) {
                results.emplace_back(entry.second);
                break;
            }
        }
    }

    std::sort(results.begin(), results.end(),
              [](const BSTSmartPointer<ObjectTypeInfo>& a, BSTSmartPointer<ObjectTypeInfo>& b) {
        return str_less<false>{}(a->GetName(), b->GetName());
    });

    return std::move(results);
}
