#include <FDGE/Binding/Papyrus/PapyrusClass.h>
#include <FDGE/Binding/Papyrus/Version.h>

using namespace FDGE::Binding::Papyrus;

namespace {
    PapyrusClass(FullyDynamicGameEngine) {
        PapyrusStaticFunction(GetVersion) {
            return new Version(0, 0, 1);
        };
    };
}
