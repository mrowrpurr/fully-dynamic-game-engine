#include <FDGE/Binding/Papyrus/PapyrusClass.h>

#include <FDGE/Binding/Papyrus/ExtendedForm.h>
#include <FDGE/Binding/Papyrus/LocationExt.h>

using namespace FDGE::Binding::Papyrus;
using namespace RE;

namespace {
    PapyrusClass(LocationExt) {
        PapyrusStaticFunction(For, BGSLocation* location) -> ExtendedForm<LocationExt> {
            return {location};
        };
    }
}
