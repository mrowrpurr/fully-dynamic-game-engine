#include <FDGE/Binding/Papyrus/SetCollection.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

namespace {
    RegisterScriptType(SetCollection)

    PapyrusClass(SetCollection) {
        PapyrusFunction(Get, SetCollection* self, Any* value) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->GetValue(value);
        };

        PapyrusFunction(Put, SetCollection* self, Any* value) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->PutValue(value);
        };

        PapyrusFunction(TryPut, SetCollection* self, Any* value) {
            if (!self) {
                return false;
            }
            return self->TryPutValue(value);
        };

        PapyrusFunction(Delete, SetCollection* self, Any* value) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Delete(value);
        };

        PapyrusFunction(Contains, SetCollection* self, Any* value) {
            if (!self) {
                return false;
            }
            return self->Contains(value);
        };

        PapyrusFunction(GetValueIteratorFrom, SetCollection* self, Any* value) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetValueIteratorFrom(value);
        };
    }
}
