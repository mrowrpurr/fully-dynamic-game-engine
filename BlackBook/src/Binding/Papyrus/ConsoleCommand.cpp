#include <FDGE/Binding/Papyrus/ConsoleCommand.h>

#include <FDGE/Binding/Papyrus/Any.h>
#include <FDGE/Binding/Papyrus/PapyrusClass.h>
#include <FDGE/Console/Console.h>

using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Console;
using namespace RE;
using namespace RE::BSScript;

ConsoleCommandExecution::ConsoleCommandExecution(Execution execution) noexcept
    : Execution(std::move(execution)) {
}

ConsoleCommand::ConsoleCommand(std::string_view ns, std::string_view name, std::string_view shortName)
    : _namespace(ns.data()), _name(name.data()), _shortName(shortName.data()) {
}

bool ConsoleCommand::Register(VMHandle handle) {
    VMHandle empty = 0;
    if (!_target.compare_exchange_strong(empty, handle)) {
        return false;
    }
    try {
        _registration = FDGE::Console::CommandRepository::GetSingleton().Register(
                FDGE::Console::CommandSpec(_namespace, {_name, _shortName},
                                           [this](FDGE::Console::Execution& execution) {
                                               HandleCommand(execution);
                                           }));
    } catch (...) {
        _target = 0;
        return false;
    }
    return true;
}

bool ConsoleCommand::Unregister(VMHandle handle) {
    if (!_target || _target != handle) {
        return false;
    }
    _registration = {};
    _target = 0;
    return true;
}

void ConsoleCommand::HandleCommand(const Execution& execution) {
    VMHandle target = _target;
    if (!target) {
        return;
    }
    auto* vm = Internal::VirtualMachine::GetSingleton();
    if (!vm) {
        return;
    }
    auto* wrapped = new ConsoleCommandExecution(execution);
    auto obj = wrapped->Bind();
    RE::FunctionArguments<ConsoleCommandExecution*> args(std::move(wrapped));
    BSFixedString event("OnExecute");
    vm->SendEvent(target, event, reinterpret_cast<IFunctionArguments*>(&wrapped));
}

namespace {
    RegisterScriptType(ConsoleCommandExecution)
    RegisterScriptType(ConsoleCommand)

    [[nodiscard]] VMHandle GetCallingHandle(BSTSmartPointer<Stack> stack) {
        if (!stack || !stack->top || !stack->top->previousFrame) {
            return {};
        }
        auto callerVar = stack->top->previousFrame->self;
        if (!callerVar.IsObject()) {
            return false;
        }
        auto caller = callerVar.GetObject();
        if (!caller) {
            return {};
        }
        return caller->GetHandle();
    }

    PapyrusClass(ConsoleCommandExecution) {
        PapyrusFunction(__HasTarget, ConsoleCommandExecution* self) {
            if (!self) {
                return false;
            }
            try {
                self->NoTarget();
            } catch (...) {
                return true;
            }
            return false;
        };

        PapyrusFunction(__HasExplicitTarget, ConsoleCommandExecution* self) {
            if (!self) {
                return false;
            }
            try {
                self->NoExplicitTarget();
            } catch (...) {
                return true;
            }
            return false;
        };

        PapyrusFunction(__SetResult, ConsoleCommandExecution* self, Any* value) {
            if (self) {
                // TODO:
            }
        };

        PapyrusFunction(GetTarget, ConsoleCommandExecution* self, std::string_view typeName, std::string_view name,
                        std::string_view shortName) -> Any* {
            if (!self) {
                return nullptr;
            }
            // TODO:
            return nullptr;
        };

        PapyrusFunction(GetParameter, ConsoleCommandExecution* self, std::string_view typeName, std::string_view name,
                        std::string_view shortName) -> Any* {
            if (!self) {
                return nullptr;
            }
            // TODO:
            return nullptr;
        };

        PapyrusFunction(HasFlag, ConsoleCommandExecution* self, std::string_view name, std::string_view shortName) {
            if (!self) {
                return false;
            }
            return self->Flag(name, shortName);
        };

        PapyrusFunction(GetNextArgument, ConsoleCommandExecution* self, std::string_view typeName,
                        std::string_view name) -> Any* {
            if (!self) {
                return nullptr;
            }
            // TODO:
            return nullptr;
        };
    }

    PapyrusClass(ConsoleCommand) {
        PapyrusStaticFunction(Create, std::string_view ns,
                              std::string_view name, std::string_view shortName) -> ConsoleCommand* {
            if (ns.empty() || (name.empty() && shortName.empty())) {
                return nullptr;
            }
            if (name.empty()) {
                name = shortName;
                shortName = "";
            }
            return new ConsoleCommand(ns, name, shortName);
        };

        PapyrusStaticFunction(Print, std::string_view text) {
            FDGE::Console::Print(text);
        };

        PapyrusFunction(__GetNamespace, ConsoleCommand* self) -> std::string_view {
            if (!self) {
                return "";
            }
            return self->GetNamespace();
        };

        PapyrusFunction(__GetName, ConsoleCommand* self) -> std::string_view {
            if (!self) {
                return "";
            }
            return self->GetName();
        };

        PapyrusFunction(__GetShortName, ConsoleCommand* self) -> std::string_view {
            if (!self) {
                return "";
            }
            return self->GetShortName();
        };

        PapyrusFunction(__HasRegistration, ConsoleCommand* self) {
            if (!self) {
                return false;
            }
            return self->GetTarget() != 0;
        };

        PapyrusFunction(RegisterForExecute, ConsoleCommand* self) {
            if (!self || self->GetTarget()) {
                return false;
            }
            return self->Register(GetCallingHandle(CurrentCallStack));
        };

        PapyrusFunction(UnregisterForExecute, ConsoleCommand* self) {
            if (!self) {
                return false;
            }
            return self->Unregister(GetCallingHandle(CurrentCallStack));
        };
    }
}
