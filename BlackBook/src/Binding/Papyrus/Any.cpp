#include <FDGE/Binding/Papyrus/Any.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>
#include <FDGE/Logger.h>

using namespace articuno;
using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace RE;

BSFixedString Any::ToString() const noexcept {
    if (IsError()) {
        std::string result("Error: ");
        result += reinterpret_cast<const Error*>(
                std::get<ScriptObjectHandle<ScriptObject>>(_value).GetObject())->GetReason();
        return result.c_str();
    } else if (_value.index() == std::variant_npos) {
        return "Empty";
    }

    return std::visit([](auto&& val) -> BSFixedString {
        using val_type = std::remove_cvref_t<decltype(val)>;

        if constexpr (std::same_as<val_type, nullptr_t>) {
            return "Empty";
        } else if constexpr (std::same_as<val_type, bool>) {
            return val ? "true" : "false";
        } else if constexpr (gluino::arithmetic<val_type>) {
            return std::to_string(val).c_str();
        } else if constexpr (std::same_as<val_type, std::string>) {
            return val;
        } else if constexpr (BSScript::is_array_v<val_type>) {
            return std::format("Array[{}]", val.size()).c_str();
        } else if constexpr (attached_object_handle<val_type>) {
            if (!val) {
                return "None";
            }

            VMTypeID type;
            if constexpr (std::same_as<typename val_type::attached_type, ActiveEffect>) {
                type = ActiveEffect::VMTYPEID;
            } else if constexpr (RE::BSScript::is_form_pointer_v<typename val_type::attached_type*>) {
                type = static_cast<VMTypeID>(val.GetObject()->GetFormType());
            } else {
                type = val.GetObject()->GetVMTypeID();
            }
            auto* vm = BSScript::Internal::VirtualMachine::GetSingleton();
            auto* policy = vm->GetObjectHandlePolicy();
            auto handle = policy->GetHandleForObject(type, val);
            BSFixedString out;
            policy->ConvertHandleToString(handle, out);
            return out.c_str();
        } else {
            auto* obj = val.GetObject();
            return obj ? obj->ToString() : "null";
        }
    }, _value);
}

bool Any::Equals(const ScriptObject* other) const noexcept {
    if (!other) {
        return false;
    }
    auto* any = dynamic_cast<const Any*>(other);
    if (!any) {
        return false;
    }
    return _value == any->_value;
}

std::size_t Any::GetHashCode() const noexcept {
    if (_value.index() == std::variant_npos) {
        return 0;
    }

    return std::visit([](auto&& val) -> std::size_t {
        using val_type = std::remove_cvref_t<decltype(val)>;

        if constexpr (std::same_as<val_type, nullptr_t>) {
            return 0;
        } else if constexpr (gluino::arithmetic<val_type>) {
            return std::hash<val_type>{}(val);
        } else if constexpr (std::same_as<val_type, std::string>) {
            return gluino::str_hash<false>{}(val);
        } else if constexpr (BSScript::is_array_v<val_type>) {
            if constexpr (gluino::arithmetic<typename val_type::value_type>) {
                auto seed = 0;
                for (auto entry : val) {
                    seed ^= std::hash<typename val_type::value_type>{}(entry) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                }
                return seed;
            } else if constexpr (std::same_as<typename val_type::value_type, std::string>) {
                auto seed = 0;
                for (auto& entry : val) {
                    seed ^= gluino::str_hash<false>{}(entry) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                }
                return seed;
            } else if constexpr (attached_object_handle<typename val_type::value_type>) {
                if constexpr (std::same_as<typename val_type::value_type::attached_type, ActiveEffect>) {
                    auto seed = 0;
                    for (auto entry : val) {
                        auto* effect = entry.GetObject();
                        seed ^= std::hash<FormID>{}(
                                effect && effect->target ? reinterpret_cast<TESForm*>(effect->target)->GetFormID() : 0);
                        seed ^= ((effect ? effect->usUniqueID : std::numeric_limits<uint16_t>::max()) + 0x9e3779b9 +
                                 (seed << 6) + (seed >> 2));
                    }
                    return seed;
                } else if constexpr (RE::BSScript::is_form_pointer_v<typename val_type::value_type::attached_type*>) {
                    auto seed = 0;
                    for (auto entry : val) {
                        auto* form = entry.GetObject();
                        seed ^= std::hash<FormID>{}(form ? form->GetFormID() : 0) + 0x9e3779b9 + (seed << 6) +
                                (seed >> 2);
                    }
                    return seed;
                } else {
                    auto seed = 0;
                    for (auto entry : val) {
                        auto* alias = entry.GetObject();
                        seed ^= std::hash<FormID>{}(alias && alias->owningQuest ? alias->owningQuest->GetFormID() : 0);
                        seed ^= ((alias ? alias->aliasID : std::numeric_limits<uint32_t>::max()) + 0x9e3779b9 +
                                 (seed << 6) + (seed >> 2));
                    }
                    return seed;
                }
            } else {
                auto seed = 0;
                for (auto& entry : val) {
                    auto* obj = entry.GetObject();
                    seed ^= (obj ? obj->GetHashCode() : 0) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                }
                return seed;
            }
        } else if constexpr (attached_object_handle<val_type>) {
            if (!val) {
                return 0;
            }
            if constexpr (std::same_as<typename val_type::attached_type, ActiveEffect>) {
                auto* effect = val.GetObject();
                auto seed = std::hash<FormID>{}(effect->target ? reinterpret_cast<TESForm*>(effect->target)->GetFormID()
                                                               : 0);
                return seed ^ (effect->usUniqueID + 0x9e3779b9 + (seed << 6) + (seed >> 2));
            } else if constexpr (RE::BSScript::is_form_pointer_v<typename val_type::attached_type*>) {
                return std::hash<FormID>{}(val.GetObject()->GetFormID());
            } else {
                auto* alias = val.GetObject();
                auto seed = std::hash<FormID>{}(alias->owningQuest ? alias->owningQuest->GetFormID() : 0);
                return seed ^ (alias->aliasID + 0x9e3779b9 + (seed << 6) + (seed >> 2));
            }
        } else {
            auto* obj = val.GetObject();
            return obj ? obj->GetHashCode() : 0;
        }
    }, _value);
}

namespace {
    RegisterScriptType(Any);

    PapyrusClass(Any) {
        PapyrusStaticFunction(Empty) -> Any* {
            return new Any();
        };

        PapyrusStaticFunction(OfBool, bool value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfInt, int32_t value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfFloat, float_t value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfString, std::string_view value) {
            return new Any(value.data());
        };

        PapyrusStaticFunction(OfObject, ScriptObject* value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfForm, AttachedObjectHandle<TESForm> value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfAlias, AttachedObjectHandle<BGSBaseAlias> value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfActiveMagicEffect, AttachedObjectHandle<ActiveEffect> value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfBools, std::vector<bool> value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfInts, std::vector<int32_t> value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfFloats, std::vector<float_t> value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfStrings, std::vector<std::string> value) {
            return new Any(std::move(value));
        };

        PapyrusStaticFunction(OfObjects, std::vector<ScriptObject*> value) {
            std::vector<ScriptObjectHandle<ScriptObject>> handles;
            handles.reserve(value.size());
            std::transform(value.begin(), value.end(), value.begin(), [](ScriptObject* obj) {
                return ScriptObjectHandle<ScriptObject>(obj);
            });
            return new Any(handles);
        };

        PapyrusStaticFunction(OfForms, std::vector<AttachedObjectHandle<TESForm>> value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfAliases, std::vector<AttachedObjectHandle<BGSBaseAlias>> value) {
            return new Any(value);
        };

        PapyrusStaticFunction(OfActiveMagicEffects, std::vector<AttachedObjectHandle<ActiveEffect>> value) {
            return new Any(value);
        };

        PapyrusStaticFunction(Error, Error* value) {
            return new Any(value);
        };

        PapyrusFunction(__IsNone, Any* self) {
            if (!self) {
                return true;
            }
            return std::holds_alternative<nullptr_t>(self->GetValue());
        };

        PapyrusFunction(__IsBool, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<bool>(self->GetValue());
        };

        PapyrusFunction(__IsInt, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<int32_t>(self->GetValue());
        };

        PapyrusFunction(__IsFloat, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<float_t>(self->GetValue());
        };

        PapyrusFunction(__IsString, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<std::string>(self->GetValue());
        };

        PapyrusFunction(__IsObject, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<ScriptObjectHandle<ScriptObject>>(self->GetValue()) && !self->IsError();
        };

        PapyrusFunction(__IsForm, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<AttachedObjectHandle<TESForm>>(self->GetValue());
        };

        PapyrusFunction(__IsAlias, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<AttachedObjectHandle<BGSBaseAlias>>(self->GetValue());
        };

        PapyrusFunction(__IsActiveMagicEffect, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<AttachedObjectHandle<ActiveEffect>>(self->GetValue());
        };

        PapyrusFunction(__IsBools, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<std::vector<bool>>(self->GetValue());
        };

        PapyrusFunction(__IsInts, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<std::vector<int32_t>>(self->GetValue());
        };

        PapyrusFunction(__IsFloats, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<std::vector<float_t>>(self->GetValue());
        };

        PapyrusFunction(__IsStrings, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<std::vector<std::string>>(self->GetValue());
        };

        PapyrusFunction(__IsObjects, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<std::vector<ScriptObjectHandle<ScriptObject>>>(self->GetValue());
        };

        PapyrusFunction(__IsForms, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<std::vector<AttachedObjectHandle<TESForm>>>(self->GetValue());
        };

        PapyrusFunction(__IsAliases, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<std::vector<AttachedObjectHandle<BGSBaseAlias>>>(self->GetValue());
        };

        PapyrusFunction(__IsActiveMagicEffects, Any* self) {
            if (!self) {
                return false;
            }
            return std::holds_alternative<std::vector<AttachedObjectHandle<ActiveEffect>>>(self->GetValue());
        };

        PapyrusFunction(__IsError, Any* self) {
            if (!self) {
                return false;
            }
            return self->IsError();
        };

        PapyrusFunction(GetBool, Any* self) -> bool {
            if (!self || !std::holds_alternative<bool>(self->GetValue())) {
                return false;
            }
            return std::get<bool>(self->GetValue());
        };

        PapyrusFunction(GetInt, Any* self) {
            if (!self || !std::holds_alternative<int32_t>(self->GetValue())) {
                return 0;
            }
            return std::get<int32_t>(self->GetValue());
        };

        PapyrusFunction(GetFloat, Any* self) {
            if (!self || !std::holds_alternative<float_t>(self->GetValue())) {
                return 0.0f;
            }
            return std::get<float_t>(self->GetValue());
        };

        PapyrusFunction(GetString, Any* self) {
            if (!self || !std::holds_alternative<std::string>(self->GetValue())) {
                return std::string("");
            }
            return std::get<std::string>(self->GetValue());
        };

        PapyrusFunction(GetObject, Any* self) -> ScriptObject* {
            if (!self || !std::holds_alternative<ScriptObjectHandle<ScriptObject>>(self->GetValue())) {
                return nullptr;
            }
            return std::get<ScriptObjectHandle<ScriptObject>>(self->GetValue());
        };

        PapyrusFunction(GetForm, Any* self) -> AttachedObjectHandle<TESForm> {
            if (!self || !std::holds_alternative<AttachedObjectHandle<TESForm>>(self->GetValue())) {
                return {};
            }
            return std::get<AttachedObjectHandle<TESForm>>(self->GetValue());
        };

        PapyrusFunction(GetAlias, Any* self) -> AttachedObjectHandle<BGSBaseAlias> {
            if (!self || !std::holds_alternative<AttachedObjectHandle<BGSBaseAlias>>(self->GetValue())) {
                return {};
            }
            return std::get<AttachedObjectHandle<BGSBaseAlias>>(self->GetValue());
        };

        PapyrusFunction(GetActiveMagicEffect, Any* self) -> AttachedObjectHandle<ActiveEffect> {
            if (!self || !std::holds_alternative<AttachedObjectHandle<ActiveEffect>>(self->GetValue())) {
                return {};
            }
            return std::get<AttachedObjectHandle<ActiveEffect>>(self->GetValue());
        };

        PapyrusFunction(GetBools, Any* self) -> std::vector<bool> {
            if (!self || !std::holds_alternative<std::vector<bool>>(self->GetValue())) {
                return {};
            }
            return std::get<std::vector<bool>>(self->GetValue());
        };

        PapyrusFunction(GetInts, Any* self) -> std::vector<int32_t> {
            if (!self || !std::holds_alternative<std::vector<int32_t>>(self->GetValue())) {
                return {};
            }
            return std::get<std::vector<int32_t>>(self->GetValue());
        };

        PapyrusFunction(GetFloats, Any* self) -> std::vector<float_t> {
            if (!self || !std::holds_alternative<float_t>(self->GetValue())) {
                return {};
            }
            return std::get<std::vector<float_t>>(self->GetValue());
        };

        PapyrusFunction(GetStrings, Any* self) -> std::vector<std::string> {
            if (!self || !std::holds_alternative<std::vector<std::string>>(self->GetValue())) {
                return {};
            }
            return std::get<std::vector<std::string>>(self->GetValue());
        };

        PapyrusFunction(GetObjects, Any* self) -> std::vector<ScriptObject*> {
            if (!self || !std::holds_alternative<std::vector<ScriptObjectHandle<ScriptObject>>>(self->GetValue())) {
                return {};
            }
            auto& handles = std::get<std::vector<ScriptObjectHandle<ScriptObject>>>(self->GetValue());
            std::vector<ScriptObject*> results;
            results.reserve(handles.size());
            std::transform(handles.begin(), handles.end(), results.begin(),
                           [](ScriptObjectHandle<ScriptObject>& handle) {
                               return handle.GetObject();
                           });
            return results;
        };

        PapyrusFunction(GetForms, Any* self) -> std::vector<AttachedObjectHandle<TESForm>> {
            if (!self || !std::holds_alternative<std::vector<AttachedObjectHandle<TESForm>>>(self->GetValue())) {
                return {};
            }
            return std::get<std::vector<AttachedObjectHandle<TESForm>>>(self->GetValue());
        };

        PapyrusFunction(GetAliases, Any* self) -> std::vector<AttachedObjectHandle<BGSBaseAlias>> {
            if (!self || !std::holds_alternative<std::vector<AttachedObjectHandle<BGSBaseAlias>>>(self->GetValue())) {
                return {};
            }
            return std::get<std::vector<AttachedObjectHandle<BGSBaseAlias>>>(self->GetValue());
        };

        PapyrusFunction(GetActiveMagicEffects, Any* self) -> std::vector<AttachedObjectHandle<ActiveEffect>> {
            if (!self || !std::holds_alternative<std::vector<AttachedObjectHandle<ActiveEffect>>>(self->GetValue())) {
                return {};
            }
            return std::get<std::vector<AttachedObjectHandle<ActiveEffect>>>(self->GetValue());
        };

        PapyrusFunction(__IsIntCompatible, Any* self) {
            if (!self) {
                return false;
            }
            if (std::holds_alternative<int32_t>(self->GetValue()) ||
                std::holds_alternative<float_t>(self->GetValue()) ||
                std::holds_alternative<bool>(self->GetValue())) {
                return true;
            }
            if (std::holds_alternative<std::string>(self->GetValue())) {
                auto& str = std::get<std::string>(self->GetValue());
                char* end;
                std::strtol(str.c_str(), &end, 10);
                return end == str.c_str() + str.size();
            }
            return false;
        };

        PapyrusFunction(__IsFloatCompatible, Any* self) {
            if (!self) {
                return false;
            }
            if (std::holds_alternative<int32_t>(self->GetValue()) ||
                std::holds_alternative<float_t>(self->GetValue()) ||
                std::holds_alternative<bool>(self->GetValue())) {
                return true;
            }
            if (std::holds_alternative<std::string>(self->GetValue())) {
                auto& str = std::get<std::string>(self->GetValue());
                char* end;
                std::strtof(str.c_str(), &end);
                return end == str.c_str() + str.size();
            }
            return false;
        };

        PapyrusFunction(__AsBool, Any* self) -> bool {
            if (!self) {
                return false;
            }
            return std::visit([](auto&& val) -> bool {
                if constexpr (RE::BSScript::is_array_v<std::remove_cvref_t<decltype(val)>> ||
                              std::same_as<std::remove_cvref_t<decltype(val)>, std::string>) {
                    return !val.empty();
                } else if constexpr (std::is_pointer_v<std::remove_cvref_t<decltype(val)>>) {
                    return val != nullptr;
                } else {
                    return static_cast<bool>(val);
                }
            }, self->GetValue());
        };

        PapyrusFunction(__AsInt, Any* self) {
            if (!self) {
                return 0;
            }
            if (std::holds_alternative<int32_t>(self->GetValue())) {
                return std::get<int32_t>(self->GetValue());
            } else if (std::holds_alternative<float_t>(self->GetValue())) {
                return static_cast<int32_t>(std::get<float_t>(self->GetValue()));
            } else if (std::holds_alternative<bool>(self->GetValue())) {
                return static_cast<int32_t>(std::get<bool>(self->GetValue()));
            } else if (std::holds_alternative<std::string>(self->GetValue())) {
                auto& str = std::get<std::string>(self->GetValue());
                char* end;
                auto result = static_cast<int32_t>(std::strtod(str.c_str(), &end));
                return end == str.c_str() + str.size() ? static_cast<int32_t>(result) : 0;
            }
            return 0;
        };

        PapyrusFunction(__AsFloat, Any* self) {
            if (!self) {
                return 0.0f;
            }
            if (std::holds_alternative<int32_t>(self->GetValue())) {
                return std::get<float_t>(self->GetValue());
            } else if (std::holds_alternative<float_t>(self->GetValue())) {
                return static_cast<float_t>(std::get<float_t>(self->GetValue()));
            } else if (std::holds_alternative<bool>(self->GetValue())) {
                return static_cast<float_t>(std::get<bool>(self->GetValue()));
            } else if (std::holds_alternative<std::string>(self->GetValue())) {
                auto& str = std::get<std::string>(self->GetValue());
                char* end;
                auto result = std::strtod(str.c_str(), &end);
                return end == str.c_str() + str.size() ? static_cast<float_t>(result) : 0.0f;
            }
            return 0.0f;
        };
    };
}
