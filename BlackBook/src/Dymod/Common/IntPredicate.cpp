#include <FDGE/Dymod/Common/IntPredicate.h>

using namespace FDGE::Dymod;
using namespace FDGE::Dymod::Common;

PredicateResult IntPredicate::TestImpl(const gluino::polymorphic_any& target) const noexcept {
    auto maybe = GetValue(target);
    if (!maybe.has_value()) {
        return PredicateResult::NotApplicable;
    }
    auto value = maybe.value();
    for (auto& range : _ranges) {
        if (value >= range.first && value <= range.second) {
            return PredicateResult::True;
        }
    }
    return PredicateResult::False;
}
