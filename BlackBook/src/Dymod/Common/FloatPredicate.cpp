#include <FDGE/Dymod/Common/FloatPredicate.h>

using namespace FDGE::Dymod;
using namespace FDGE::Dymod::Common;

PredicateResult FloatPredicate::TestImpl(const gluino::polymorphic_any& target) const noexcept {
    auto maybe = GetValue(target);
    if (!maybe.has_value()) {
        return PredicateResult::NotApplicable;
    }
    auto value = maybe.value();
    for (auto& range: _ranges) {
        if (((_inclusiveMinimum && value >= range.first) || (!_inclusiveMinimum && value > range.first)) &&
                ((_inclusiveMaximum && value <= range.second) || (!_inclusiveMaximum && value < range.second))) {
            return PredicateResult::True;
        }
    }
    return PredicateResult::False;
}
