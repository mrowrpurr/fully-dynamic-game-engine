#include <FDGE/Dymod/RuleModule.h>

#include <FDGE/Dymod/Action.h>
#include <FDGE/Dymod/DynamicEvent.h>
#include <FDGE/Dymod/Predicate.h>
#include <FDGE/Dymod/Registry.h>
#include <FDGE/Dymod/Rule.h>
#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>

using namespace FDGE;
using namespace FDGE::Dymod;

namespace {
    class FormRuleModule : public RuleModule {
    public:

    protected:
        void Apply(const DynamicEvent& event) const noexcept override {

        }

    private:
    };

    DymodRegister(FormRuleModule, u8"form");
}
