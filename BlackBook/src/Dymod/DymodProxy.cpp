#include <FDGE/Dymod/DymodProxy.h>

#include <articuno/articuno.h>
#include <articuno/archives/ryml/ryml.h>

using namespace articuno;
using namespace articuno::ryml;
using namespace FDGE;
using namespace FDGE::Config;
using namespace FDGE::Dymod;
using namespace FDGE::Util;

DymodProxy::DymodProxy(std::string_view name) : _directory(std::string("Data\\Dymods\\") + name.data()) {
    Refresh(false);
    StartWatching();
}

const DymodProxy DymodProxy::Create(std::string_view name) {
    return DymodProxy(name);
}

void DymodProxy::StartWatching() const {
    _watcher = std::make_unique<FilesystemWatcher>(_directory, [this](const auto&, auto) {
        Refresh(true);
    });
}

void DymodProxy::StopWatching() const {
    _watcher.release();
}

void DymodProxy::Refresh(bool) const {
    auto manifestPath = _directory;
    manifestPath += std::filesystem::path::preferred_separator;
    manifestPath += "manifest.dymod.yaml";

    std::ifstream manifest(manifestPath);
    if (manifest.bad()) {
        // TODO: Handle error;
    }
    yaml_source<> ar(manifest);
    auto mod = std::make_shared<Mod>();
    ar >> *mod;

    _data = std::move(mod);
}

void DymodProxy::Save() const {
    // Do nothing, we do not save Dymods.
}
