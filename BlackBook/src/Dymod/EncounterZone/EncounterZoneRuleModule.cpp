#include <FDGE/Dymod/RuleModule.h>

#include <FDGE/Dymod/Action.h>
#include <FDGE/Dymod/DynamicEvent.h>
#include <FDGE/Dymod/Predicate.h>
#include <FDGE/Dymod/Registry.h>
#include <FDGE/Dymod/Rule.h>
#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>

using namespace FDGE;
using namespace FDGE::Dymod;

namespace {
    class EncounterZoneRuleModule : public RuleModule {
    public:

    protected:
        void Apply(const DynamicEvent& event) const noexcept override {

        }

    private:
    };

    DymodRegister(EncounterZoneRuleModule, u8"encounter-zone");
}
