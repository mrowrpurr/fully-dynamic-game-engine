#include <FDGE/Console/CommandRepository.h>

#include <FDGE/Hook/FunctionHook.h>
#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>
#include <FDGE/Util/Maps.h>

#include "../FDGEConfig.h"

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Hook;
using namespace FDGE::Util;
using namespace gluino;
using namespace phmap;
using namespace RE;

namespace {
    [[nodiscard]] auto& GetVanillaCommands() noexcept {
        static istr_flat_hash_map<std::string_view, const SCRIPT_FUNCTION*> commands;
        return commands;
    }

    [[nodiscard]] auto& GetCommandsLock() noexcept {
        static std::recursive_mutex lock;
        return lock;
    }

    [[nodiscard]] auto& GetCommandsByName() noexcept {
        static istr_flat_hash_map<std::string, istr_flat_hash_map<std::string, CommandSpec>> commands;
        return commands;
    }

    [[nodiscard]] auto& GetCommandsByID() noexcept {
        static flat_hash_map<uint64_t, CommandSpec*> commands;
        return commands;
    }

    uint64_t NextID{1};

    void HookCompileAndRun() {
        static FunctionHook<void(Script*, ScriptCompiler*, COMPILER_NAME, TESObjectREFR*)> CompileAndRun(
                RE::Offset::Script::CompileAndRun, [](auto* self, auto* compiler, auto compilerName, auto* target) {
                    std::unique_lock lock(GetCommandsLock());
                    Parser parser(self->text);
                    if (!parser.IsValid()) {
                        // Command does not match syntax, pass to original handler for error handling.
                        lock.unlock();
                        CompileAndRun(self, compiler, compilerName, target);
                        return;
                    }
                    auto found = GetCommandsByName().find(parser.GetCommand());
                    if (found == GetCommandsByName().end()) {
                        auto vanilla = GetVanillaCommands().find(parser.GetCommand());
                        if (vanilla != GetVanillaCommands().end() && !vanilla->second->referenceFunction) {
                            Logger::Debug("Enabling call to non-reference command '{}' in reference context.",
                                          parser.GetCommand());
                            target = nullptr;
                        }
                        // No matching command found, pass to original handler, it may be a vanilla command.
                        lock.unlock();
                        CompileAndRun(self, compiler, compilerName, target);
                    } else {
                        const CommandSpec* cmd;
                        if (parser.GetNamespace().empty()) {
                            if (found->second.size() == 1) {
                                cmd = &found->second.begin()->second;
                            } else {
                                std::string validCommands;
                                for (auto& entry : found->second) {
                                    if (!validCommands.empty()) {
                                        validCommands += ", ";
                                    }
                                    validCommands += entry.second.GetNamespace();
                                    validCommands += "::";
                                    validCommands += parser.GetCommand();
                                }
                                auto err = std::format("Command is ambiguous, use the namespace notation to invoke the "
                                                       "correct command. Possible commands are: {}.", validCommands);
                                RE::ConsoleLog::GetSingleton()->Print(err.c_str());
                                return;
                            }
                        } else {
                            auto nsResult = found->second.find(parser.GetNamespace());
                            if (nsResult == found->second.end()) {
                                // Command does not match syntax, pass to original handler for error handling since
                                // this command necessarily cannot match a vanilla one.
                                lock.unlock();
                                CompileAndRun(self, compiler, compilerName, target);
                                return;
                            } else {
                                cmd = &nsResult->second;
                            }
                        }
                        try {
                            lock.unlock();
                            Execution ex(parser);
                            cmd->GetHandler()(ex);
                        } catch (const std::exception& e) {
                            RE::ConsoleLog::GetSingleton()->Print(e.what());
                        }
                    }
                });
    }

    OnSKSELoaded {
        if (!FDGE::FDGEConfig::GetProxy()->GetDebug().GetModules().IsConsoleCommandsEnabled()) {
            Logger::Warn("Console commands module is not enabled, skipping hooks.");
            return;
        }

        HookCompileAndRun();

        auto* start = SCRIPT_FUNCTION::GetFirstConsoleCommand();
        for (std::size_t i = 0; i != SCRIPT_FUNCTION::Commands::kConsoleCommandsEnd; ++i) {
            auto& fn = start[i];
            GetVanillaCommands()[fn.functionName] = &fn;
            if (fn.shortName && fn.shortName[0] != '\0') {
                GetVanillaCommands()[fn.shortName] = &fn;
            }
        }

        start = SCRIPT_FUNCTION::GetFirstScriptCommand();
        for (std::size_t i = 0; i != SCRIPT_FUNCTION::Commands::kScriptCommandsEnd; ++i) {
            auto& fn = start[i];
            GetVanillaCommands()[fn.functionName] = &fn;
            if (fn.shortName && fn.shortName[0] != '\0') {
                GetVanillaCommands()[fn.shortName] = &fn;
            }
        }
    }
}

CommandRegistration CommandRepository::Register(const CommandSpec& command) {
    return {RegisterImpl(command)};
}

CommandRegistration CommandRepository::Register(const CommandSpec&& command) {
    return Register(command);
}

void CommandRepository::RegisterForever(const CommandSpec& command) {
    RegisterImpl(command);
}

void CommandRepository::RegisterForever(const CommandSpec&& command) {
    RegisterForever(command);
}

uint64_t CommandRepository::RegisterImpl(const CommandSpec& command) {
    std::unique_lock lock(GetCommandsLock());
    CommandSpec* spec = nullptr;
    for (auto name : command.GetNames()) {
        if (str_equal_to<false>()(name, "clear") || GetVanillaCommands().contains(name)) {
            Logger::Error("Console command name '{}' conflicts with a vanilla script function.", name);
            Unregister(command);
            return false;
        }
        auto result = GetCommandsByName()[name].try_emplace(command.GetNamespace(), command);
        if (!result.second) {
            Logger::Error("Console command name '{}::{}' conflicts with an existing console command.",
                          command.GetNamespace(), name);
            Unregister(command);
            return false;
        }
        spec = &result.first->second;
    }
    auto id = NextID++;
    if (spec) {
        GetCommandsByID()[id] = spec;
    }
    return id;
}

void CommandRepository::Unregister(const CommandSpec& command) {
    for (auto name : command.GetNames()) {
        auto result = GetCommandsByName().find(name);
        if (result == GetCommandsByName().end()) {
            continue;
        }
        result->second.erase(command.GetNamespace());
        if (result->second.empty()) {
            GetCommandsByName().erase(result);
        }
    }
}

void CommandRepository::Unregister(uint64_t id) {
    std::unique_lock lock(GetCommandsLock());
    auto result = GetCommandsByID().find(id);
    if (result == GetCommandsByID().end()) {
        return;
    }
    const auto& command = *result->second;
    Unregister(command);
    GetCommandsByID().erase(result);
}

CommandRepository& CommandRepository::GetSingleton() noexcept {
    static CommandRepository instance;
    return instance;
}
