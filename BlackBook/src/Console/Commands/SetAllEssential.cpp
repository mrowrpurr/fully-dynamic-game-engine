#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

#include <FDGE/Skyrim/Actors.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Skyrim;
using namespace RE;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"SetAllEssential", "saessent"}, [](Execution& execution) {
                        execution.NoExplicitTarget();
                        auto value = execution.NextPositional<bool>("value");

                        for (auto actor : Actors::GetSingleton()) {
                            if (!actor) {
                                continue;
                            }
                            if (value) {
                                actor->boolFlags.set(Actor::BOOL_FLAGS::kEssential);
                            } else {
                                actor->boolFlags.reset(Actor::BOOL_FLAGS::kEssential);
                            }
                            auto* base = actor->GetActorBase()->As<TESActorBaseData>();
                            if (value) {
                                base->actorData.actorBaseFlags.set(ACTOR_BASE_DATA::Flag::kEssential);
                            } else {
                                base->actorData.actorBaseFlags.reset(ACTOR_BASE_DATA::Flag::kEssential);
                            }
                        }
                    }));
        });
    }
}
