#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>
#include <FDGE/Skyrim/Actors.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Skyrim;
using namespace RE;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"KillNearbyActors", "killnear"}, [](Execution& execution) {
                        auto* target = execution.OptionalTarget<Actor*>(PlayerCharacter::GetSingleton());
                        auto hostileOnly = execution.Flag("hostileOnly", "h");
                        auto combatOnly = execution.Flag("combatOnly", "c");
                        double_t units = 350;
                        auto rangeUnits = execution.OptionalParameter<double_t>("units");
                        auto rangeMeters = execution.OptionalParameter<double_t>("meters");
                        auto rangeFeet = execution.OptionalParameter<double_t>("feet");
                        auto rangeYards = execution.OptionalParameter<double_t>("yards");
                        int count = 0;
                        count += rangeUnits.has_value() + rangeFeet.has_value() + rangeMeters.has_value() +
                                rangeYards.has_value();
                        if (count > 2) {
                            throw std::invalid_argument(
                                    "Only one of parameters '--units', '--meters', '--feet', or '--yards' should be "
                                    "provided.");
                        }
                        if (rangeUnits.has_value()) {
                            units = rangeUnits.value();
                        } else if (rangeMeters.has_value()) {
                            units = rangeMeters.value() * 70.0280112;
                        } else if (rangeFeet.has_value()) {
                            units = rangeFeet.value() * 21.3333333;
                        } else if (rangeYards.has_value()) {
                            units = rangeFeet.value() * 21.3333333 * 3;
                        }
                        if (units < 0) {
                            throw std::invalid_argument("Range argument cannot be negative.");
                        }

                        for (auto actor : Actors::GetSingleton()) {
                            if (!actor) {
                                continue;
                            }
                            if (actor.get() != PlayerCharacter::GetSingleton() && actor.get() != target &&
                                actor->GetPosition().GetDistance(target->GetPosition()) <= units &&
                                (!hostileOnly || actor->IsHostileToActor(target)) &&
                                (!combatOnly || actor->IsInCombat())) {
                                actor->KillImpl(actor.get(), 0.0f, true, false);
                                actor->DecRefCount();
                            }
                        }
                    }));
        });
    }
}
