#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Plugin.h>
#include <FDGE/Skyrim/FormType.h>
#include <FDGE/System.h>
#include <FDGE/Util/Maps.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Util;
using namespace gluino;
using namespace RE;

namespace {
    inline void HandleForm(bool useRegex, bool useExact, bool usePrefix, bool useSuffix, bool useEditorID,
                           bool useFullName, const std::regex& pattern, BSFixedString text, TESForm* form,
                           istr_btree_map<std::string_view, std::vector<std::string>>& results) {
        if (!form) {
            return;
        }

        BSFixedString matchedEditorID;
        if (!useFullName) {
            auto edid = BSFixedString(form->GetFormEditorID());
            if (useRegex) {
                if (std::regex_search(edid.data(), pattern)) {
                    matchedEditorID = edid;
                }
            } else {
                if ((useExact && edid == text) ||
                    (usePrefix && std::strncmp(edid.c_str(), text.c_str(), text.size()) == 0) ||
                    (useSuffix && str_equal_to<false>{}(edid.c_str() + edid.size() - text.size(),
                                                        text.c_str())) ||
                    (!usePrefix && !useSuffix && !useExact && strstr(edid.c_str(), text.c_str()))) {
                    matchedEditorID = edid;
                }
            }
        }
        BSFixedString matchedName;
        if (!useEditorID) {
            auto* fullNameForm = skyrim_cast<TESFullName*>(form);
            if (!fullNameForm) {
                return;
            }
            auto fullName = BSFixedString(fullNameForm->GetFullName());
            if (useRegex) {
                if (std::regex_search(fullName.data(), pattern)) {
                    matchedName = fullName;
                }
            } else {
                if ((useExact && fullName == text) ||
                    (usePrefix && std::strncmp(fullName.c_str(), text.c_str(), text.size()) == 0) ||
                    (useSuffix && str_equal_to<false>{}(
                            fullName.c_str() + fullName.size() - text.size(), text.c_str())) ||
                    (!usePrefix && !useSuffix && !useExact &&
                     strstr(fullName.c_str(), text.c_str()))) {
                    matchedName = fullName;
                }
            }
        }
        auto recordName = FDGE::Skyrim::FormType(form->GetFormType()).GetRecordName();
        auto entryList = results.try_emplace(recordName, std::vector<std::string>{});
        if (entryList.second) {
            entryList.first->second.reserve(32);
        }
        if (matchedEditorID.empty()) {
            if (matchedName.empty()) {
                return;
            }
            entryList.first->second.emplace_back(
                    std::format(R"({}: {:X} (Name="{}"))", recordName, form->GetFormID(), matchedName.c_str()));
        } else if (matchedName.empty()) {
            entryList.first->second.emplace_back(
                    std::format(R"({}: {:X} (EditorID="{}"))", recordName, form->GetFormID(), matchedEditorID.c_str()));
        } else {
            entryList.first->second.emplace_back(
                    std::format(R"({}: {:X} (Name="{}", EditorID="{}"))", recordName, form->GetFormID(),
                                matchedName.c_str(), matchedEditorID.c_str()));
        }
    }

    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"search"}, [](Execution& execution) {
                        execution.NoExplicitTarget();

                        auto useRegex = execution.Flag("regex", "r");
                        auto usePrefix = execution.Flag("prefix", "p");
                        auto useSuffix = execution.Flag("suffix", "s");
                        auto useExact = execution.Flag("exact", "x");

                        if (static_cast<int>(useRegex) + usePrefix + useSuffix + useExact > 1) {
                            throw std::invalid_argument(
                                    "Only one of 'regex', 'prefix', 'suffix', or 'exact' flags can be provided.");
                        }

                        auto formType = execution.OptionalParameter<Skyrim::FormType>("type", FormType::None);
                        bool useEditorID = execution.Flag("edid", "e");
                        bool useFullName = execution.Flag("name", "n");
                        if (useEditorID && useFullName) {
                            throw std::invalid_argument(
                                    "Flags '--edid' (or '-e') and '--name' (or '-n') cannot be used together.");
                        }
                        auto text = BSFixedString(execution.NextPositional<std::string>("text"));
                        std::regex pattern(text.c_str(), std::regex::icase | std::regex::optimize);
                        if (text.empty()) {
                            Print("Text to search for must not be empty.");
                            return;
                        }

                        istr_btree_map<std::string_view, std::vector<std::string>> results;

                        if (useRegex) {
                            Print("Beginning search, this may take a while...");
                        } else {
                            Print("Beginning search...");
                        }
                        if (formType == FormType::None) {
                            auto allForms = TESForm::GetAllForms();
                            BSReadLockGuard lock(allForms.second.get());
                            for (auto& entry: *allForms.first) {
                                HandleForm(useRegex, useExact, usePrefix, useSuffix, useEditorID, useFullName, pattern,
                                       text, entry.second, results);
                            }
                        } else {
                            auto& forms = TESDataHandler::GetSingleton()->GetFormArray(formType);
                            for (auto& entry : forms) {
                                HandleForm(useRegex, useExact, usePrefix, useSuffix, useEditorID, useFullName, pattern,
                                       text, entry, results);
                            }
                        }

                        for (auto& result : results) {
                            for (auto& line : result.second) {
                                Print(line);
                            }
                        }
                    }));
        });
    }
}
