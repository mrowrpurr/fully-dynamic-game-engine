#include <FDGE/Binding/Papyrus/TestSuite.h>
#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Console;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"runtests"}, [](Execution& execution) {
                        execution.NoExplicitTarget();

                        bool allAsserts = execution.Flag("allasserts", "a");
                        std::string suitePattern = execution.OptionalParameter<std::string>("suites", "");
                        std::string testPattern = execution.OptionalParameter<std::string>("tests", "");

                        auto* player = RE::PlayerCharacter::GetSingleton();
                        if (!player || !player->GetParentCell()) {
                            Print("Tests must be run after starting or loading a game.");
                            return;
                        }

                        if (suitePattern.empty() && testPattern.empty()) {
                            Print("Running all tests...");
                        } else {
                            Print("Running selected tests...");
                        }
                        TestSuite::RunAll(allAsserts, true, "", suitePattern, testPattern);
                    }));
        });
    }
}
