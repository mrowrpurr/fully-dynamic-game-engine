#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Dymod/DymodLoader.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Dymod;
using namespace gluino;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"DymodDisable", "modoff"}, [](Execution& execution) {
                        execution.NoExplicitTarget();
                        auto id = execution.NextPositional<std::string>("DymodID");
                        auto noRefresh = execution.Flag("norefresh", "n");

                        std::u8string encodedID = string_cast<char8_t>(id);

                        auto& rules = DymodLoader::GetSingleton().GetLoadOrderRules();
                        rules.GetBlacklist()[encodedID] = std::make_pair(
                                semantic_version(0, 0, 0), semantic_version(std::numeric_limits<uint16_t>::max(),
                                                                            std::numeric_limits<uint16_t>::max(),
                                                                            std::numeric_limits<uint16_t>::max()));

                        if (!noRefresh) {
                            Print("Dymod {} disabled, refreshing mods.", id);
                            DymodLoader::GetSingleton().Refresh();
                        } else {
                            Print("Dymod {} flagged disabled, will not be loaded on next refresh.", id);
                        }
                    }));
        });
    }
}
