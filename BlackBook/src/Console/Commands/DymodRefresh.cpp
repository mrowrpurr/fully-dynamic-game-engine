#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Dymod/DymodLoader.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Dymod;
using namespace gluino;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"DymodRefresh", "dyr"}, [](Execution& execution) {
                        execution.NoExplicitTarget();
                        DymodLoader::GetSingleton().Refresh();
                        Print("Dymods refreshed, {0} mods loaded.", DymodLoader::GetSingleton().GetLoadOrder().size());
                    }));
        });
    }
}
