#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Dymod/DymodLoader.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Dymod;
using namespace gluino;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"DymodEnable", "modon"}, [](Execution& execution) {
                        execution.NoExplicitTarget();
                        std::string id = execution.NextPositional<std::string>("DymodID");
                        bool noRefresh = execution.Flag("norefresh", "n");
                        bool override = execution.Flag("override", "o");

                        std::u8string encodedID = string_cast<char8_t>(id);

                        auto& rules = DymodLoader::GetSingleton().GetLoadOrderRules();
                        rules.GetBlacklist().erase(encodedID);
                        if (override) {
                            rules.GetOverrideEnable()[encodedID] = std::make_pair(
                                    semantic_version(0, 0, 0), semantic_version(std::numeric_limits<uint16_t>::max(),
                                                                                std::numeric_limits<uint16_t>::max(),
                                                                                std::numeric_limits<uint16_t>::max()));
                        }

                        if (!noRefresh) {
                            Print("Dymod {} enabled, refreshing mods.", id);
                            DymodLoader::GetSingleton().Refresh();
                        } else {
                            Print("Dymod {} flagged enabled, will not be loaded on next refresh.", id);
                        }
                    }));
        });
    }
}