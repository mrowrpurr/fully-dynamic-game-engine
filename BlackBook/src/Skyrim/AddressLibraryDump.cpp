#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>

#include "../FDGEConfig.h"
#include "../ThirdParty/versionlibdb.h"

using namespace FDGE;

namespace {
    OnSKSELoaded {
        if (!FDGEConfig::GetProxy()->GetDebug().GetModules().IsAddressLibraryDumpEnabled()) {
            Logger::Trace("Address Library ID dump is disabled.");
            return;
        }
        Logger::Info("Address Library ID dump is enabled, dumping version databases...");

        FDGE_WIN_CHAR_TYPE myDocumentsPath[MAX_PATH];
        auto result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, myDocumentsPath);
        if (result != S_OK) {
            Logger::Error("Unable to determine documents path, skipping Address Library dump.");
            return;
        }
        std::filesystem::path basePath(myDocumentsPath);
        basePath /= "My Games";
        basePath /= "Skyrim Special Edition";
        basePath /= "Address Library";
        std::filesystem::create_directories(basePath);

        std::regex versionLibPattern(R"(versionlib-((\d+)-(\d+)-(\d+)-(\d+)).bin)",
                                     std::regex::optimize | std::regex::icase);

        int currentMajor;
        int currentMinor;
        int currentPatch;
        int currentBuild;
        VersionDb().GetExecutableVersion(currentMajor, currentMinor, currentPatch, currentBuild);

        std::filesystem::directory_iterator dir("Data\\SKSE\\Plugins");
        for (auto& entry: dir) {
            std::smatch match;
            auto filename = entry.path().filename().string();
            if (!std::regex_match(filename, match, versionLibPattern)) {
                continue;
            }

            VersionDb db;
            int major = std::stoi(match[2].str());
            int minor = std::stoi(match[3].str());
            int patch = std::stoi(match[4].str());
            int build = std::stoi(match[5].str());
            if (major == currentMajor && minor == currentMinor && patch == currentPatch && build == currentBuild) {
                Logger::Info("Found version database file '{}' (match for current Skyrim runtime version).",
                                   match[0].str());
            } else {
                Logger::Info("Found version database file '{}'.", match[0].str());
            }
            db.Load(major, minor, patch, build);

            std::filesystem::path outputPath(basePath);
            outputPath /= "versionlib-offsets-";
            outputPath += match[1].str();
            outputPath += ".txt";
            auto outputPathStr = outputPath.string();
            Logger::Info("Dumping version database to '{}'.", outputPathStr);
            db.Dump(outputPathStr);
        }

        Logger::Info("Dump of Address Library IDs complete.");
    }
}
