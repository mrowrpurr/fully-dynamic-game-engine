#include <FDGE/Skyrim/FormExtension.h>

#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>
#include <FDGE/Hook/FunctionHook.h>
#include <FDGE/Util/Maps.h>

#include "../FDGEConfig.h"

using namespace FDGE;
using namespace FDGE::Hook;
using namespace FDGE::Skyrim;
using namespace FDGE::Util;
using namespace RE;

namespace {
    Util::parallel_flat_hash_map<RE::FormID, BSFixedString> formEditorIDs;

    void HookEditorIDs() {
        static FunctionHook<const char*(const RE::TESForm*)> GetFormEditorID(
                10879, [](auto* self) {
                    if (!self) {
                        return "";
                    }
                    BSFixedString result;
                    formEditorIDs.if_contains(self->GetFormID(), [&](const BSFixedString& value) {
                        result = value;
                    });
                    return result.c_str();
                });

        static FunctionHook<bool(RE::TESForm*, const char*)> SetFormEditorID(
                10926, [](auto* self, const auto* editorID) {
                    if (!self) {
                        return false;
                    }
                    bool isEmpty = editorID == nullptr || editorID[0] == '\0';
                    auto allForms = TESForm::GetAllFormsByEditorID();
                    RE::BSWriteLockGuard lock(allForms.second.get());
                    auto oldResult = formEditorIDs.find(self->GetFormID());
                    if (oldResult != formEditorIDs.end()) {
                        allForms.first->erase(oldResult->second);
                        if (isEmpty) {
                            formEditorIDs.erase(oldResult);
                            return true;
                        }
                    }
                    BSFixedString interned(editorID);
                    allForms.first->emplace(interned, self);
                    formEditorIDs[self->GetFormID()] = interned;
                    return true;
                });
    }

    void ReserveEditorIDs() {
        auto reservation = FDGEConfig::GetProxy()->GetPerformance().GetEditorIDReservation();
        formEditorIDs.reserve(FDGEConfig::GetProxy()->GetPerformance().GetEditorIDReservation());
        Logger::Debug("Reserved {} entries for editor ID population.", reservation);
    }

    OnSKSELoaded {
        if (!FDGEConfig::GetProxy()->GetDebug().GetModules().IsEditorIDPreservationEnabled()) {
            Logger::Warn("Editor ID preservation module is disabled, skipping editor ID hooks.");
            return;
        }

        HookEditorIDs();
        ReserveEditorIDs();

        static auto onPluginsUnloaded = System().Listen([](const SkyrimPluginsUnloadedEvent&) {
            formEditorIDs.clear();
            ReserveEditorIDs();
        });

        static auto event = System().Listen([](const SkyrimPluginsLoadedEvent&) {
            Logger::Info("Preserved {} editor IDs in loaded mods.", formEditorIDs.size());
        });
    }
}
