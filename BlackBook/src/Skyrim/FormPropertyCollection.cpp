#include <FDGE/Skyrim/FormPropertyCollection.h>

#include <articuno/archives/ryml/ryml.h>

using namespace FDGE;
using namespace FDGE::Hook;
using namespace FDGE::Skyrim;
using namespace articuno::ryml;

FormPropertyCollection::FormPropertyCollection(std::string_view name) :
        _name(name.data()), SerializationHook(std::string("FormPropertyCollection::") + name.data()) {
}

void FormPropertyCollection::OnNewGame() {
    _properties.clear();
}

void FormPropertyCollection::OnGameSaved(std::ostream& out) {
    yaml_sink ar(out);
    ar << _properties;
}

void FormPropertyCollection::OnGameLoaded(std::istream& in) {
    _properties.clear();
    yaml_source ar(in);
    ar >> _properties;
}

std::optional<std::shared_ptr<FormProperty>> FormPropertyCollection::Get(
        const PortableID& form, std::string_view key) const noexcept {
    std::shared_ptr<FormProperty> result;
    if (_properties.if_contains(
            form, [&](Util::istr_parallel_flat_hash_map<std::string, std::shared_ptr<FormProperty>, 0>& map) {
                map.if_contains(key, [&](std::shared_ptr<FormProperty>& value) {
                    result = value;
                });
            })) {
        return {};
    }
    if (result) {
        return result;
    }
    return {};
}

void FormPropertyCollection::Clear(const PortableID& form) {
    _properties.erase(form);
}

void FormPropertyCollection::Erase(const PortableID& form, std::string_view key) {
    _properties.erase_if(
            form, [key](Util::istr_parallel_flat_hash_map<std::string, std::shared_ptr<FormProperty>, 0>& map) {
                map.erase(key);
                return map.empty();
        });
}
