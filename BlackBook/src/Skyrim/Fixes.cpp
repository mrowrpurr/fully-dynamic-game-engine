#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>
#include <FDGE/Hook/CallHook.h>

#include "../FDGEConfig.h"

using namespace FDGE;
using namespace FDGE::Hook;

namespace {
    OnSKSELoading {
        if (!FDGEConfig::GetProxy()->GetDebug().GetModules().IsFixesEnabled()) {
            Logger::Warn("Fixes module disabled, skipping fix hooks.");
            return;
        }

        Logger::Info("Registering fix hooks...");
        // TODO: This hook currently causes problems with SKSE's release builds, and other strange behavior.
//        if (FDGEConfig::GetProxy()->GetFixes().IsRemoteDesktopEnabled()) {
//            Logger::Debug("Creating hook to enable remote desktop...");
//            static CallHook<int(int)> GetSystemMetrics(
//                    36547, 0xA9, [](int metric) {
//                        if (GetSystemMetrics(metric)) {
//                            Logger::Trace("Bypassing block on Remote Desktop...");
//                        }
//                        return 0;
//                    });
//        }

        if (FDGEConfig::GetProxy()->GetFixes().IsZeroWeightProjectilesEnabled()) {
            Logger::Debug("Setting minimum projectile weight to zero...");
            // TODO:
        }

        Logger::Info("Fix hooks registered.");
    }
}