scriptName FormExtTestSuite extends AutoTestSuite hidden

function TestGetFormObjectNonExistent()
    Assert.IsFormNone(FormExt.GetFormObject(Game.GetPlayer(), "-bad class name"))
endFunction

function TestGetFormObjectNoneForm()
    Assert.IsFormNone(FormExt.GetFormObject(None, "Script"))
endFunction

function TestGetFormObjectGood()
    Form dbQuest = Game.GetForm(0x1EA5C)
    Assert.IsFormNotNone(dbQuest)
    Form dbQuestObject = FormExt.GetFormObject(dbQuest, "DarkBrotherhood")
    Assert.IsFormNotNone(dbQuestObject)
    Assert.IsFormNotNone(dbQuestObject as DarkBrotherhood)
endFunction
