scriptName DynamicArrayTestSuite extends AutoTestSuite hidden

function TestDefaultConstructor()
    DynamicArray arr = DynamicArray.Create()
    Assert.IsTrue(arr.Empty)
    Assert.AreIntsEqual(0, arr.Size)
    Assert.AreIntsEqual(0, arr.Capacity)
endFunction

function TestReserveConstructor()
    DynamicArray arr = DynamicArray.Create(8)
    Assert.IsTrue(arr.Empty)
    Assert.AreIntsEqual(0, arr.Size)
    Assert.AreIntsEqual(8, arr.Capacity)
endFunction

function TestSize()
    DynamicArray arr = DynamicArray.Create()
    Assert.AreIntsEqual(0, arr.Size)
    arr.Append(Any.Empty())
    Assert.AreIntsEqual(1, arr.Size)
    arr.Append(Any.Empty())
    Assert.AreIntsEqual(2, arr.Size)
    arr.PopBack()
    Assert.AreIntsEqual(1, arr.Size)
endFunction

function TestEmpty()
    DynamicArray arr = DynamicArray.Create()
    Assert.IsTrue(arr.Empty)
    arr.Append(Any.Empty())
    Assert.IsFalse(arr.Empty)
    arr.Append(Any.Empty())
    Assert.IsFalse(arr.Empty)
    arr.PopBack()
    Assert.IsFalse(arr.Empty)
    arr.PopBack()
    Assert.IsTrue(arr.Empty)
endFunction

function TestReserveExpansion()
    DynamicArray arr = DynamicArray.Create()
    Assert.AreIntsEqual(0, arr.Capacity)
    arr.Reserve(16)
    Assert.AreIntsEqual(16, arr.Capacity)
endFunction

function TestReserveStatusQuo()
    DynamicArray arr = DynamicArray.Create()
    arr.Resize(16)
    Int capacity = arr.Capacity
    Assert.IsTrue(capacity >= 16, "Capacity expected to be at least 16.")
    arr.Reserve(8)
    Assert.IsTrue(capacity >= 16, "Reserving less than current capacity should not have no effect.")
endFunction

function TestResizeExpansion()
    DynamicArray arr = DynamicArray.Create()
    Assert.IsTrue(arr.Empty)
    Assert.AreIntsEqual(0, arr.Size)
    arr.Resize(8)
    Assert.IsFalse(arr.Empty)
    Assert.AreIntsEqual(8, arr.Size)
endFunction

function TestResizeShrinkage()
    DynamicArray arr = DynamicArray.Create()
    arr.Resize(8)
    arr.Append(Any.OfInt(10))
    Assert.AreIntsEqual(9, arr.Size)
    arr.Resize(8)
    Assert.AreIntsEqual(8, arr.Size)
    Assert.IsObjectNone(arr.Get(7))
endFunction
