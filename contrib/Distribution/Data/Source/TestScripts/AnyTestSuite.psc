scriptName AnyTestSuite extends AutoTestSuite hidden

function TestEmpty()
    Any a = Any.Empty()
    Assert.IsObjectNotNone(a)
    Assert.IsTrue(a.IsNone)
endFunction

function TestBool()
    Any a = Any.OfBool(true)
    Assert.IsTrue(a.IsBool)
    Assert.IsTrue(a.GetBool())
    a = Any.OfBool(false)
    Assert.IsObjectNotNone(a)
    Assert.IsTrue(a.IsBool)
    Assert.IsFalse(a.GetBool())
endFunction

function TestForm()
    Any a = Any.OfForm(Game.GetPlayer())
    Assert.IsTrue(a.IsForm)
    Assert.AreFormsEqual(Game.GetPlayer(), a.GetForm())
endFunction

function TestFormExtension()
    DarkBrotherhood db = FormExt.GetFormObject(Game.GetForm(0x1EA5C), "DarkBrotherhood") as DarkBrotherhood
    Assert.IsFormNotNone(db)
    Any a = Any.OfForm(db)
    Assert.IsTrue(a.IsForm)
    Form result = a.GetForm()
    Assert.IsFormNotNone(result)
    Assert.IsFormNotNone(result as DarkBrotherhood)
endFunction

function TestAlias()
    Alias al = AliasExt.GetAliasByID(Game.GetForm(0x1EA5C) as Quest, 1)
    Any a = Any.OfAlias(al)
    Assert.IsTrue(a.IsAlias)
    Assert.AreAliasesEqual(al, a.GetAlias())
endFunction

function TestAliasExtension()
    Alias al = AliasExt.GetAliasByID(Game.GetForm(0x68D73) as Quest, 9)
    CWDisableDuringSiegeALIASScript stables = AliasExt.GetAliasObject(al, "CWDisableDuringSiegeALIASScript") as CWDisableDuringSiegeALIASScript
    Assert.IsAliasNotNone(stables)
    Any a = Any.OfAlias(stables)
    Assert.IsTrue(a.IsAlias)
    Alias result = a.GetAlias()
    Assert.IsAliasNotNone(result)
    Assert.IsAliasNotNone(result as CWDisableDuringSiegeALIASScript)
endFunction
