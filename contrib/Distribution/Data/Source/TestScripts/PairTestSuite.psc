scriptName PairTestSuite extends AutoTestSuite hidden

function TestDefaultConstructor()
    Pair p = Pair.Create()
    Assert.IsObjectNotNone(p)
    Assert.IsObjectNone(p.First)
    Assert.IsObjectNone(p.Second)
endFunction

function TestFirstOnlyConstructor()
    Pair p = Pair.Create(Any.OfInt(10))
    Assert.IsObjectNotNone(p)
    Assert.AreObjectsEqual(p.First, Any.OfInt(10))
    Assert.IsObjectNone(p.Second)
endFunction

function TestStandardConstructor()
    Pair p = Pair.Create(Any.OfInt(10), Any.OfString("Foobar"))
    Assert.IsObjectNotNone(p)
    Assert.AreObjectsEqual(p.First, Any.OfInt(10))
    Assert.AreObjectsEqual(p.Second, Any.OfString("Foobar"))
endFunction
