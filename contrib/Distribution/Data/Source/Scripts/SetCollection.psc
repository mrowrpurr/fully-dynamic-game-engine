scriptName SetCollection extends Collection hidden

Any function Get(Any value) native

Any function Put(Any value) native

Bool function TryPut(Any value) native

Bool function Contains(Any value) native

Any function Delete(Any value) native

ValueIterator function GetValueIteratorFrom(Any value) native
