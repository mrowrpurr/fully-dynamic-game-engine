scriptName Stack extends ScriptObject hidden

Stack function Create() global native

function Push(Any value) native

Any function Pop() native

Any function Peek() native

Bool property Empty
    Bool function get()
        return Size == 0
    endFunction
endProperty

Int property Size
    Int function get()
        return __GetSize()
    endFunction
endProperty
Int function __GetSize() native
