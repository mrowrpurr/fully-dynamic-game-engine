scriptName KeyValueIterator extends ValueIterator hidden

Any property MapKey
    Any function get()
        return __GetMapKey()
    endFunction
endProperty
Any function __GetMapKey() native

Pair property Entry
    Pair function get()
        return __GetEntry()
    endFunction
endProperty
Pair function __GetEntry() native

Any function GetMapKeyAndNext() native

Any function NextAndGetMapKey() native

Pair function GetEntryAndNext() native

Pair function NextAndGetEntry() native
