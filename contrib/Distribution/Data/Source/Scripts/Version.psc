scriptName Version extends ScriptObject hidden

Version function Create(Int major, Int minor = 0, Int patch = 0) global native

Int property Major
    Int function get()
        return __GetMajor()
    endFunction
endProperty
Int function __GetMajor() native

Int property Minor
    Int function get()
        return __GetMinor()
    endFunction
endProperty
Int function __GetMinor() native

Int property Patch
    Int function get()
        return __GetPatch()
    endFunction
endProperty
Int function __GetPatch() native

Bool function NewerThan(Version other) native

Bool function OlderThan(Version other) native
