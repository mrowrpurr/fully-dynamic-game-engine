scriptName Deque extends RandomAccessCollection hidden

Deque function Create() global native

function Prepend(Any value) native

function Append(Any value) native

Any function PopFront() native

Any function PopBack() native
