scriptName ConsoleCommand extends ScriptObject hidden

ConsoleCommand function Create(String namespace, String name, String shortName = "") global native

function Print(String text) global native

String property Namespace
    String function get()
        return __GetNamespace()
    endFunction
endProperty
String function __GetNamespace() native

String property Name
    String function get()
        return __GetName()
    endFunction
endProperty
String function __GetName() native

String property ShortName
    String function get()
        return __GetShortName()
    endFunction
endProperty
String function __GetShortName() native

Bool property HasRegistration
    Bool function get()
        return __HasRegistration()
    endFunction
endProperty
Bool function __HasRegistration() native

Bool function RegisterForExecute() native

Bool function UnregisterForExecute() native
