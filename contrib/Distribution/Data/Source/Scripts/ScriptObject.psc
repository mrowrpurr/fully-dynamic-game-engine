scriptName ScriptObject hidden

ScriptObject function CreateNewThis() global native

Error function Initialize(ScriptObject object, String variableName, Any value) global native

Int[] function GetHashCode() native

Bool function Equals(ScriptObject other) native

String function ToString() native
