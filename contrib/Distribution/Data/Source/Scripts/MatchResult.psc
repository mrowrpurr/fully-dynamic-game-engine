scriptName MatchResult extends ScriptObject hidden

String property Value
    String function get()
        return __GetValue()
    endFunction
endProperty
String function __GetValue() native

Int property StartIndex
    Int function get()
        return __GetStartIndex()
    endFunction
endProperty
Int function __GetStartIndex() native

Int property EndIndex
    Int function get()
        return __GetEndIndex()
    endFunction
endProperty
Int function __GetEndIndex() native

Int property MatchLength
    Int function get()
        return __GetMatchLength()
    endFunction
endProperty
Int function __GetMatchLength() native
