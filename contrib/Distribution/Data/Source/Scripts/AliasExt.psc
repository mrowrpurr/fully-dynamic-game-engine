scriptName AliasExt hidden

Alias function GetAliasByName(Quest owner, String name) global native

Alias function GetAliasByID(Quest owner, Int id) global native

Alias function GetAliasObject(Alias target, String className) global native
