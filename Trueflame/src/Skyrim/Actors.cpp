#include <FDGE/Skyrim/Actors.h>

using namespace FDGE;
using namespace FDGE::Skyrim;
using namespace gluino;
using namespace RE;

namespace {
    REL::Relocation<uintptr_t*> ActorData(REL::ID(400315));
    REL::Relocation<void(uintptr_t, RE::Actor*&)> GetLoadedActor(REL::ID(17201));
}

Actors Actors::_singleton;

Actors::iterator Actors::end() const noexcept {
    return iterator(*reinterpret_cast<uint32_t*>(*ActorData + 64));
}

optional_ptr<Actor> Actors::iterator::operator*() const noexcept {
    RE::Actor* actor{nullptr};
    GetLoadedActor(*reinterpret_cast<std::size_t*>(*ActorData + 48) + 4 * _i, actor);
    return actor;
}

optional_ptr<Actor> Actors::iterator::operator->() const noexcept {
    return **this;
}

bool Actors::iterator::operator==(const iterator& other) const noexcept {
    return _i == other._i || _i >= _singleton.end()._i;
}
