#include <FDGE/Skyrim/PapyrusLogger.h>

#include <FDGE/Logger.h>

using namespace FDGE;
using namespace FDGE::Skyrim;
using namespace RE;
using namespace RE::BSScript;

BSEventNotifyControl PapyrusLogger::ProcessEvent(const LogEvent* event, BSTEventSource<LogEvent>*) {
    if (!_enabled || !event || !event->errorMsg) {
        return BSEventNotifyControl::kContinue;
    }
    switch (event->severity) {
        case level_type::kFatal:
            Logger::Critical(event->errorMsg);
            break;
        case level_type::kError:
            Logger::Error(event->errorMsg);
            break;
        case level_type::kWarning:
            Logger::Warn(event->errorMsg);
            break;
        case level_type::kInfo:
            Logger::Info(event->errorMsg);
            break;
        default:
            break;
    }
    return BSEventNotifyControl::kContinue;
}
