#include <FDGE/Skyrim/PortableID.h>

#include <gluino/map.h>
#include <FDGE/Logger.h>
#include <FDGE/Skyrim/FormIndex.h>

using namespace gluino;
using namespace FDGE;
using namespace FDGE::Skyrim;
using namespace RE;

namespace {
    template <class Collection>
    void InitializeEditorIDs(std::vector<std::string>& destination, Collection&& editorIDs) {
        if (!editorIDs.size()) {
            Logger::Error("Attempt to create editor ID-based portable ID with empty list of editor IDs; ID will point "
                          "to null.");
            return;
        }
        destination.reserve(editorIDs.size());
        bool success = false;
        bool failed = false;
        for (auto& editorID : editorIDs) {
            if (editorID.empty()) {
                failed = true;
                continue;
            }
            destination.emplace_back(editorID.data());
            success = true;
        }
        if (!success) {
            Logger::Error("Attempt to create editor ID-based portable ID with all empty editor IDs; ID will point to "
                          "null.");
        } else if (failed) {
            Logger::Warn("Editor ID-based portable ID created with an empty editor ID in list; empty editor ID will be "
                         "ignored.");
        }
    }

    template <class Collection>
    void InitializeFileNames(std::vector<std::string>& destination, Collection&& files) {
        if (!files.size()) {
            Logger::Error("Attempt to create form ID-based portable ID with empty list of files; ID will point "
                          "to null.");
            return;
        }
        destination.reserve(files.size());
        bool success = false;
        bool failed = false;
        for (auto& file : files) {
            if (file.empty()) {
                failed = true;
                continue;
            }
            destination.emplace_back(file.data());
            success = true;
        }
        if (!success) {
            Logger::Error("Attempt to create form ID-based portable ID with all file names; ID will point to "
                          "null.");
        } else if (failed) {
            Logger::Warn("Form ID-based portable ID created with an empty file name in list; empty file name will "
                         "be ignored.");
        }
    }

    template <class Collection>
    void InitializeFormIDs(std::vector<FormID>& destination, Collection&& formIDs) {
        if (!formIDs.size()) {
            Logger::Error("Attempt to create form ID-based portable ID with empty list of form IDs; ID will point "
                          "to null.");
            return;
        }
        destination.reserve(formIDs.size());
        bool success = false;
        bool failed = false;
        for (auto& formID : formIDs) {
            if (!formID) {
                failed = true;
                continue;
            }
            destination.emplace_back(formID);
            success = true;
        }
        if (!success) {
            Logger::Error("Attempt to create form ID-based portable ID with all zero form IDs; ID will point to "
                          "null.");
        } else if (failed) {
            Logger::Warn("Form ID-based portable ID created with a zero form ID in the list; null form ID will "
                         "be ignored.");
        }
    }
}

PortableID::PortableID(const RE::TESForm* form) {
    if (!form) {
        return;
    }

    auto* editorID = form->GetFormEditorID();
    if (editorID != nullptr && editorID[0] != '\0') {
        _names.emplace_back(editorID);
        return;
    }

    auto formID = form->GetFormID();
    auto modIndex = formID & 0xFF000000;
    if (modIndex == 0xFE000000) {
        _ids.emplace_back(formID & 0xFFFFFF);
    } else {
        _ids.emplace_back(formID & 0xFFF);
    }
    if (modIndex != 0xFF000000 && form->sourceFiles.array && !form->sourceFiles.array->empty()) {
        _names.emplace_back(form->GetFile(0)->fileName);
    }
}

PortableID::PortableID(std::string_view editorID) {
    if (editorID.empty()) {
        Logger::Error("Attempt to create editor ID-based portable ID with empty editor ID; ID will point to null.");
    } else {
        _names.emplace_back(editorID.data());
    }
}

PortableID::PortableID(std::initializer_list<std::string_view> editorIDs) {
    InitializeEditorIDs(_names, editorIDs);
}

PortableID::PortableID(std::initializer_list<std::string>&& editorIDs) {
    InitializeEditorIDs(_names, editorIDs);
}

PortableID::PortableID(const std::vector<std::string_view>& editorIDs) {
    InitializeEditorIDs(_names, editorIDs);
}

PortableID::PortableID(const std::vector<std::string>& editorIDs) {
    InitializeEditorIDs(_names, editorIDs);
}

PortableID::PortableID(std::string_view file, RE::FormID formID) {
    if (file.empty()) {
        Logger::Error("Attempt to create form ID-based portable ID with form ID '{}' with an empty filename; "
                      "ID will point to null.", formID);
        return;
    }
    if (!formID) {
        Logger::Error("Attempt to create a form ID-based portable ID with a form ID of 0; ID will point to null.");
        return;
    }
    _names.emplace_back(file.data());
    _ids.emplace_back(formID);
}

PortableID::PortableID(std::initializer_list<std::string_view> files, RE::FormID formID) {
    InitializeFileNames(_names, files);
    if (!formID) {
        Logger::Error("Attempt to create a form ID-based portable ID with a form ID of 0; ID will point to null.");
        return;
    }
    _ids.emplace_back(formID);
}

PortableID::PortableID(std::initializer_list<std::string>&& files, RE::FormID formID) {
    InitializeFileNames(_names, files);
    if (!formID) {
        Logger::Error("Attempt to create a form ID-based portable ID with a form ID of 0; ID will point to null.");
        return;
    }
    _ids.emplace_back(formID);
}

PortableID::PortableID(const std::vector<std::string_view>& files, RE::FormID formID) {
    InitializeFileNames(_names, files);
    if (!formID) {
        Logger::Error("Attempt to create a form ID-based portable ID with a form ID of 0; ID will point to null.");
        return;
    }
    _ids.emplace_back(formID);
}

PortableID::PortableID(const std::vector<std::string>& files, RE::FormID formID) {
    InitializeFileNames(_names, files);
    if (!formID) {
        Logger::Error("Attempt to create a form ID-based portable ID with a form ID of 0; ID will point to null.");
        return;
    }
    _ids.emplace_back(formID);
}

PortableID::PortableID(std::initializer_list<std::string_view> files, std::initializer_list<RE::FormID> formIDs) {
    InitializeFileNames(_names, files);
    InitializeFormIDs(_ids, formIDs);
}

PortableID::PortableID(std::initializer_list<std::string>&& files, std::initializer_list<RE::FormID> formIDs) {
    InitializeFileNames(_names, files);
    InitializeFormIDs(_ids, formIDs);
}

PortableID::PortableID(const std::vector<std::string_view>& files, std::vector<RE::FormID> formIDs) {
    InitializeFileNames(_names, files);
    InitializeFormIDs(_ids, formIDs);
}

PortableID::PortableID(const std::vector<std::string>& files, std::vector<RE::FormID> formIDs) {
    InitializeFileNames(_names, files);
    InitializeFormIDs(_ids, formIDs);
}

PortableID::PortableID(std::string_view file, std::initializer_list<RE::FormID> formIDs) {
    if (file.empty()) {
        Logger::Error("Attempt to create form ID-based portable ID with form IDs with an empty filename; "
                      "ID will point to null.");
        return;
    }
    InitializeFormIDs(_ids, formIDs);
    _names.emplace_back(file.data());
}

PortableID::PortableID(std::string_view file, std::vector<RE::FormID> formIDs) {
    if (file.empty()) {
        Logger::Error("Attempt to create form ID-based portable ID with form IDs with an empty filename; "
                      "ID will point to null.");
        return;
    }
    InitializeFormIDs(_ids, formIDs);
    _names.emplace_back(file.data());
}

PortableID::PortableID(RE::FormID saveGameFormID) {
    if (!saveGameFormID) {
        Logger::Error("Attempt to create a form ID-based portable ID with a form ID of 0; ID will point to null.");
        return;
    }
    _ids.emplace_back(saveGameFormID);
}

PortableID::PortableID(std::initializer_list<RE::FormID> saveGameFormIDs) {
    InitializeFormIDs(_ids, saveGameFormIDs);
}

PortableID::PortableID(std::vector<RE::FormID> saveGameFormIDs) {
    InitializeFormIDs(_ids, saveGameFormIDs);
}

bool PortableID::IsNull() const noexcept {
    if (!_names.empty()) {
        return false;
    }
    for (auto id : _ids) {
        if (id) {
            return false;
        }
    }
    return true;
}

std::string PortableID::ToString() const {
    if (IsNull()) {
        return "null";
    }

    std::ostringstream result;
    bool added = false;
    for (auto& name : _names) {
        if (!added) {
            added = true;
        } else {
            result << ',';
        }
        result << std::hex << name;
    }

    if (!_ids.empty()) {
        result << ":";
        added = false;
        for (auto& id : _ids) {
            if (!added) {
                added = true;
            } else {
                result << ',';
            }
            result << std::hex << id;
        }
    }

    return result.str();
}

optional_ptr<TESForm> PortableID::Lookup(FormType formType) {
    FormIndex index;
    return index.Lookup(*this, formType);
}

optional_ptr<const TESForm> PortableID::Lookup(FormType formType) const {
    const FormIndex index;
    return index.Lookup(*this, formType);
}

std::strong_ordering PortableID::operator<=>(const PortableID& other) const noexcept {
    for (std::size_t i = 0; i < _names.size() && i < other._names.size(); ++i) {
        auto result = _names[i] <=> other._names[i];
        if (result != std::strong_ordering::equal) {
            return result;
        }
    }
    if (_names.size() != other._names.size()) {
        return _names.size() <=> other._names.size();
    }
    for (std::size_t i = 0; i < _ids.size() && i < other._ids.size(); ++i) {
        auto result = _ids[i] <=> other._ids[i];
        if (result != std::strong_ordering::equal) {
            return result;
        }
    }
    if (_ids.size() != other._ids.size()) {
        return _ids.size() <=> other._ids.size();
    }
    return std::strong_ordering::equal;
}

std::optional<PortableID> PortableID::Parse(std::string_view input) {
    gluino::str_equal_to<false> equals;
    if (equals(input, "null")) {
        return {};
    }

    std::istringstream in(input.data());
    std::string names;
    std::string ids;
    std::string remaining;
    std::getline(in, names, ':');
    std::getline(in, ids, ':');
    std::getline(in, remaining, ':');
    if (!remaining.empty()) {
        Logger::Error("Failed to parse prospective portable ID '{}'.", input);
        return {};
    }

    static std::regex namesPattern(R"(([^,:]+)(?:,([^,:]+))*)", std::regex::icase | std::regex::optimize);
    std::vector<std::string> inputNames;
    std::smatch namesMatch;
    if (!names.empty()) {
        if (!std::regex_match(names, namesMatch, namesPattern)) {
            Logger::Error("Failed to parse prospective portable ID '{}'.", input);
            return {};
        }
        for (std::size_t i = 1; i < namesMatch.size(); ++i) {
            if (namesMatch[i].matched) {
                inputNames.emplace_back(namesMatch[i].str());
            }
        }
    }

    if (!ids.empty()) {
        static std::regex idsPattern(R"(([0-9a-z]+)(?:,([0-9a-z]+))*)", std::regex::icase | std::regex::optimize);
        std::vector<FormID> inputIds;
        std::smatch idsMatch;
        if (!std::regex_match(ids, idsMatch, idsPattern)) {
            Logger::Error("Failed to parse prospective portable ID '{}'.", input);
            return {};
        }
        for (std::size_t i = 1; i < idsMatch.size(); ++i) {
            if (idsMatch[i].matched) {
                inputIds.emplace_back(std::stoul(idsMatch[i].str(), nullptr, 16));
            }
        }

        if (inputNames.empty()) {
            return PortableID(inputIds);
        } else {
            return PortableID(inputNames, inputIds);
        }
    } else {
        return PortableID(inputNames);
    }
}

std::size_t std::hash<PortableID>::operator()(const PortableID& value) const noexcept {
    std::hash<std::string> namesHasher;
    std::size_t result = 0;
    auto names = value.GetFileNames();
    if (names.empty()) {
        names = value.GetEditorIDs();
    }
    for (auto& name : names) {
        result ^= namesHasher(name) + 0x9e3779b9 + (result << 6) + (result >> 2);
    }

    std::hash<RE::FormID> idsHasher;
    for (auto& id : value.GetFormIDs()) {
        result ^= idsHasher(id) + 0x9e3779b9 + (result << 6) + (result >> 2);
    }

    return result;
}
