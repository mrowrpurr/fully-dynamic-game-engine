#include <FDGE/Util/RecursiveMutex.h>

using namespace FDGE::Util;

RecursiveMutex::RecursiveMutex() noexcept {
}

RecursiveMutex::~RecursiveMutex() noexcept {
}

void RecursiveMutex::lock() const noexcept {
    return _lock.lock();
}

bool RecursiveMutex::try_lock() const noexcept {
    return _lock.try_lock();
}

void RecursiveMutex::unlock() const noexcept {
    return _lock.unlock();
}
