#include <FDGE/Util/SharedTimedMutex.h>

using namespace FDGE::Util;

SharedTimedMutex::SharedTimedMutex() noexcept {
}

SharedTimedMutex::~SharedTimedMutex() noexcept {
}

void SharedTimedMutex::lock() const noexcept {
    return _lock.lock();
}

void SharedTimedMutex::lock_shared() const noexcept {
    return _lock.lock_shared();
}

bool SharedTimedMutex::try_lock() const noexcept {
    return _lock.try_lock();
}

bool SharedTimedMutex::try_lock_shared() const noexcept {
    return _lock.try_lock_shared();
}

void SharedTimedMutex::unlock() const noexcept {
    return _lock.unlock();
}

void SharedTimedMutex::unlock_shared() const noexcept {
    return _lock.unlock_shared();
}

bool SharedTimedMutex::try_lock_for_impl(const std::chrono::milliseconds& timeout_duration) {
    return _lock.try_lock_for(timeout_duration);
}

bool SharedTimedMutex::try_lock_until_impl(
        const std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>& timeout_time) {
    return _lock.try_lock_until(timeout_time);
}

bool SharedTimedMutex::try_lock_shared_for_impl(const std::chrono::milliseconds& timeout_duration) {
    return _lock.try_lock_shared_for(timeout_duration);
}

bool SharedTimedMutex::try_lock_shared_until_impl(
        const std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>& timeout_time) {
    return _lock.try_lock_shared_until(timeout_time);
}
