#include <FDGE/Hook/SKSESerializationHook.h>

#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>
#include <FDGE/Util/Maps.h>

using namespace articuno;
using namespace articuno::ryml;
using namespace FDGE;
using namespace FDGE::Hook;
using namespace SKSE;

namespace {
    constexpr std::size_t BufferSize = 4096;

    uint32_t recordID;

    volatile bool Initialized{false};

    template <class T>
    struct SaveEntry {
        articuno_serde(ar) {
            ar <=> kv(Name, "name");
            ar <=> kv(Content, "content");
        }

        std::string Name;
        std::stringstream Content;
    };

    static inline Util::istr_parallel_flat_hash_map<std::string, std::function<void(std::ostream&)>> _saveHooks;
    static inline Util::istr_parallel_flat_hash_map<std::string, std::function<void(std::istream&)>> _loadHooks;
    static inline Util::parallel_flat_hash_map<std::string, std::function<void()>> _revertHooks;

    void InitializeCosaves() {
        static std::atomic_bool initialized{false};
        static std::latch latch(1);
        if (initialized.exchange(true)) {
            latch.wait();
            return;
        }

        auto pluginData = FDGE::detail::Plugin::Lookup();
        if (pluginData.is_null()) {
            throw std::logic_error("Auto-discovery of plugin's serialization unique ID for serialization hooks requires"
                                   " use FuDGE Trueflame Plugin declaration.");
        }
        uint32_t uniqueID = pluginData->SKSESerializationPluginID;
        if (!uniqueID) {
            throw std::logic_error(
                    "Auto-discovery of plugins' serialization unique ID for serialization hooks requires"
                    " Plugin declaration to explicitly specify a unique ID.");
        }
        uint32_t serializationRecordID = pluginData->SKSESerializationRecordID;
        if (!serializationRecordID) {
            throw std::logic_error(
                    "Auto-discovery of plugins' serialization record ID for serialization hooks requires"
                    " Plugin declaration to have a non-empty ID.");
        }
        SKSE::GetSerializationInterface()->SetUniqueID(uniqueID);

        SKSE::GetSerializationInterface()->SetSaveCallback([](SKSE::SerializationInterface* serialization) {
            if (serialization->OpenRecord(recordID, 1)) {
                std::stringstream out;
                yaml_sink<> ar(out);
                for (auto& hook: _saveHooks) {
                    SaveEntry<std::string&> entry;
                    entry.Name = hook.first;
                    hook.second(entry.Content);
                    entry.Content.seekg(0, std::ios::beg);
                    ar << entry;
                }
                auto output = out.str();
                serialization->WriteRecordData(output.c_str(), static_cast<uint32_t>(output.size()));
            } else {
                Logger::Error("Unable to open record for save hooks.");
            }
        });

        SKSE::GetSerializationInterface()->SetLoadCallback([](SKSE::SerializationInterface* serialization) {
            std::uint32_t type;
            std::uint32_t size;
            std::uint32_t version;
            while (serialization->GetNextRecordInfo(type, version, size)) {
                // Skip irrelevant records to find the save hook one.
                if (type != recordID) {
                    continue;
                }

                // Read in all record data.
                std::stringstream in;
                char buffer[BufferSize];
                std::uint32_t count;
                while ((count = serialization->ReadRecordData(buffer, BufferSize)) > 0) {
                    in.read(buffer, count);
                }
                in.seekg(0, std::stringstream::end);
                if (in.tellg() == 0) {
                    // No contents.
                    break;
                }
                in.seekg(0, std::stringstream::beg);

                yaml_source ar(in);
                while (!in.eof()) {
                    SaveEntry<std::string> entry;
                    ar >> entry;
                    entry.Content.seekg(0, ::std::ios_base::beg);
                    _loadHooks.if_contains(entry.Name, [&entry](auto& hook) {
                        hook(entry.Content);
                    });
                }
            }
        });

        SKSE::GetSerializationInterface()->SetRevertCallback([](SKSE::SerializationInterface*) {
            for (auto& hook : _revertHooks) {
                hook.second();
            }
        });

        latch.count_down();
    }
}

SKSESerializationHook::SKSESerializationHook(std::string_view name) : _name(name.data()) {
    Initialize();
}

SKSESerializationHook::SKSESerializationHook(std::string&& name) : _name(std::move(name)) {
    Initialize();
}

SKSESerializationHook::~SKSESerializationHook() noexcept {
    _saveHooks.erase(_name);
    _loadHooks.erase(_name);
    _revertHooks.erase(_name);
}

void SKSESerializationHook::Initialize() {
    InitializeCosaves();
    if (!_saveHooks.try_emplace(_name, [this](std::ostream& out) {
        OnGameSaved(out);
    }).second) {
        throw std::invalid_argument(std::format("SKSE serialization hook name '{}' is already registered.", _name));
    }
    _loadHooks.try_emplace(_name, [this](std::istream& in) {
        OnGameLoaded(in);
    });
    _revertHooks.try_emplace(_name, [this]() { OnRevert(); });
}

void SKSESerializationHook::OnRevert() {
}

void SKSESerializationHook::OnGameSaved(std::ostream&) {
}

void SKSESerializationHook::OnGameLoaded(std::istream&) {
}
