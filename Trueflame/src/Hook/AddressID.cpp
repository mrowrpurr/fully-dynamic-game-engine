#include <FDGE/Hook/AddressID.h>

#include <FDGE/Plugin.h>

using namespace FDGE::Hook;

namespace {
    bool isAE;

    OnSKSELoading {
        isAE = LoadInterface.RuntimeVersion()[1] >= 6;
    }
}

bool AddressID::IsAE() noexcept {
    return isAE;
}
