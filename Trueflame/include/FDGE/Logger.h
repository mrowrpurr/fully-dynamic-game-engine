#pragma once

#include <format>
#include <source_location>

#include <debugapi.h>
#include <gluino/string_cast.h>
#include <spdlog/async.h>
#include <spdlog/sinks/null_sink.h>
#include <spdlog/sinks/win_eventlog_sink.h>
#include <spdlog/sinks/msvc_sink.h>
#include <spdlog/sinks/rotating_file_sink.h>

#include "Plugin.h"
#include "Config/PluginConfig.h"
#include "Config/Proxy.h"

namespace FDGE {
    /**
     * Implements a customizable and powerful logging system with dynamic configuration updates.
     */
    class Logger {
    public:
        /**
         * Initializes the logger with configuration from a proxy.
         *
         * <p>
         * This will load data from the configuration managed by the proxy. It also registers as a listener so that it
         * can refresh logging configuration when the proxy signals an update. The name of the logging outputs will be
         * based on the plugin name stored in the Trueflame plugin declaration, which is required for this call.
         * </p>
         *
         * @tparam T The type of data stored in the proxy; must conform to the <code>DebugConfig</code> class from
         * Trueflame.
         * @param proxy The proxy with the the data to load.
         */
        template <class T>
        static void Initialize(Config::Proxy<T>& proxy) {
            auto plugin = detail::Plugin::Lookup();
            if (!plugin) {
                throw std::logic_error("Initializing the logger without an explicit plugin name requires the "
                                       "SKSEPlugin_Version structure to be present and include a plugin name.");
            }
            auto* name = reinterpret_cast<const char*>(reinterpret_cast<uintptr_t>(plugin.get()) + 0x8);
            if (name[0] == '\0') {
                throw std::logic_error("Initializing the logger without an explicit plugin name requires the "
                                       "SKSEPlugin_Version structure to include a plugin name.");
            }
            auto pluginName = gluino::claim(gluino::string_cast<FDGE_WIN_CHAR_TYPE>(name));
            _instance.reset(new Logger(proxy, pluginName));
        }

        /**
         * Initializes the logger with configuration from a proxy.
         *
         * <p>
         * This will load data from the configuration managed by the proxy. It also registers as a listener so that it
         * can refresh logging configuration when the proxy signals an update. The name of the logging outputs must be
         * given explicitly in this call, but this allows the use of non-Trueflame plugin declarations..
         * </p>
         *
         * @tparam T The type of data stored in the proxy; must conform to the <code>DebugConfig</code> class from
         * Trueflame.
         * @param proxy The proxy with the the data to load.
         * @param pluginName The name to assume for the plugin when generating logging outputs.
         */
        template <class T>
        static void Initialize(Config::Proxy<T>& proxy, FDGE_WIN_STRING_VIEW_TYPE pluginName) {
            _instance.reset(new Logger(proxy, pluginName));
        }

        template <class... Args>
        struct Log {
            inline explicit Log(spdlog::level::level_enum level, std::string_view format, Args&& ... args,
                                std::source_location location = std::source_location::current()) {
                LogMessage(location, level, format, std::forward<Args>(args)...);
            }
        };

        template <class... Args>
        Log(spdlog::level::level_enum, std::string_view, Args&& ...) -> Log<Args...>;

        template <class... Args>
        struct Critical {
            Critical() = delete;
            Critical(const Critical&) = delete;
            Critical(Critical&&) = delete;

            constexpr explicit Critical(std::string_view format, Args&& ... args,
                                        std::source_location location = std::source_location::current()) {
                LogMessage(location, spdlog::level::critical, format, std::forward<Args>(args)...);
            }
        };

        template <class... Args>
        Critical(std::string_view, Args&& ...) -> Critical<Args...>;

        template <class... Args>
        struct Error {
            Error() = delete;
            Error(const Error&) = delete;
            Error(Error&&) = delete;

            constexpr explicit Error(std::string_view format, Args&& ... args,
                                     std::source_location location = std::source_location::current()) {
                LogMessage(location, spdlog::level::err, format, std::forward<Args>(args)...);
            }
        };

        template <class... Args>
        Error(std::string_view, Args&& ...) -> Error<Args...>;

        template <class... Args>
        struct Warn {
            Warn() = delete;
            Warn(const Warn&) = delete;
            Warn(Warn&&) = delete;

            constexpr explicit Warn(std::string_view format, Args&& ... args,
                                    std::source_location location = std::source_location::current()) {
                LogMessage(location, spdlog::level::warn, format, std::forward<Args>(args)...);
            }
        };

        template <class... Args>
        Warn(std::string_view, Args&& ...) -> Warn<Args...>;

        template <class... Args>
        struct Info {
            Info() = delete;
            Info(const Info&) = delete;
            Info(Info&&) = delete;

            inline explicit Info(std::string_view format, Args&& ... args,
                                 std::source_location location = std::source_location::current()) {
                LogMessage(location, spdlog::level::info, format, std::forward<Args>(args)...);
            }
        };

        template <class... Args>
        Info(std::string_view, Args&& ...) -> Info<Args...>;

        template <class... Args>
        struct Debug {
            Debug() = delete;
            Debug(const Debug&) = delete;
            Debug(Debug&&) = delete;

            constexpr explicit Debug(std::string_view format, Args&& ... args,
                                     std::source_location location = std::source_location::current()) {
                LogMessage(location, spdlog::level::debug, format, std::forward<Args>(args)...);
            }
        };

        template <class... Args>
        Debug(std::string_view, Args&& ...) -> Debug<Args...>;

        template <class... Args>
        struct Trace {
            Trace() = delete;
            Trace(const Trace&) = delete;
            Trace(Trace&&) = delete;

            constexpr explicit Trace(std::string_view format, Args&& ... args,
                                     std::source_location location = std::source_location::current()) {
                LogMessage(location, spdlog::level::trace, format, std::forward<Args>(args)...);
            }
        };

        template <class... Args>
        Trace(std::string_view, Args&& ...) -> Trace<Args...>;

    private:
        template <class T>
        inline explicit Logger(Config::Proxy<T>& proxy, FDGE_WIN_STRING_VIEW_TYPE pluginName)
                : _pluginName(pluginName.data()) {
            proxy.ListenForever([this](const Config::DataRefreshedEvent&) {
                _updateHandler(true);
            });
            _updateHandler = [&](bool reinitializing) {
                Update(proxy, reinitializing);
            };

            _updateHandler(false);
        }

        template <class... Args>
        static void LogMessage(std::source_location location, spdlog::level::level_enum level,
                                      std::string_view format, Args&&... args) noexcept {
            if (!_instance || ((!_instance->_debuggerLogger || level < _instance->_debuggerLogger->level()) &&
                               (!_instance->_fileLogger || level < _instance->_fileLogger->level()) &&
                               (!_instance->_eventViewerLogger || level < _instance->_eventViewerLogger->level()))) {
                return;
            }
            std::string msg = std::format(format, args...);
            spdlog::source_loc loc{location.file_name(), static_cast<int>(location.line()), location.function_name()};
            if (_instance->_debuggerLogger) {
                _instance->_debuggerLogger->log(loc, level, msg.c_str());
            }
            if (_instance->_fileLogger) {
                _instance->_fileLogger->log(loc, level, msg.c_str());
            }
            if (_instance->_eventViewerLogger) {
                _instance->_eventViewerLogger->log(loc, level, msg.c_str());
            }
        }

        template <class T>
        void Update(Config::Proxy<T>& proxy, bool reinitializing) {
            const auto& debugConfig = proxy.Get()->GetDebug();

            if (debugConfig.IsAsyncLogging() && debugConfig.GetLoggingQueueSize() > 0 &&
                debugConfig.GetLoggingThreadCount() > 0) {
                spdlog::init_thread_pool(debugConfig.GetLoggingQueueSize(), debugConfig.GetLoggingThreadCount());
            } else {
                spdlog::details::registry::instance().set_tp(nullptr);
            }
            spdlog::flush_every(debugConfig.GetFlushPeriod());

            // Flush pending log entries after dropping loggers.
            spdlog::drop_all();
            if (_debuggerLogger) {
                _debuggerLogger->flush();
            }
            if (_fileLogger) {
                _fileLogger->flush();
            }
            if (_eventViewerLogger) {
                _eventViewerLogger->flush();
            }

            _nullLogger = std::make_shared<spdlog::logger>(
                    "nullLogger", std::make_shared<spdlog::sinks::null_sink_mt>());
            spdlog::set_default_logger(_nullLogger);

            // Setup debugger logging.
            if (debugConfig.GetDebuggerLogger().IsEnabled() &&
                (!debugConfig.GetDebuggerLogger().IsRequireDebuggerPresent() || IsDebuggerPresent())) {
                if (debugConfig.IsAsyncLogging()) {
                    if (debugConfig.GetDebuggerLogger().IsNonBlocking()) {
                        _debuggerLogger = spdlog::create_async_nb<spdlog::sinks::msvc_sink_mt>("debuggerLogger");
                    } else {
                        _debuggerLogger = spdlog::create_async<spdlog::sinks::msvc_sink_mt>("debuggerLogger");
                    }
                } else {
                    _debuggerLogger = std::make_shared<spdlog::logger>("debuggerLogger",
                                                                       std::make_shared<spdlog::sinks::msvc_sink_mt>());
                }
                _debuggerLogger->set_pattern(debugConfig.GetDebuggerLogger().GetLogPattern());
                _debuggerLogger->set_level(debugConfig.GetDebuggerLogger().GetLogLevel());
                _debuggerLogger->flush_on(debugConfig.GetDebuggerLogger().GetFlushLevel());
                spdlog::register_logger(_debuggerLogger);
                if (debugConfig.GetDefaultLogger() == Config::DefaultLogger::debuggerLogger) {
                    spdlog::set_default_logger(_debuggerLogger);
                }
            } else {
                _debuggerLogger = nullptr;
            }

            // Setup event viewer logging.
            if (debugConfig.GetEventViewerLogger().IsEnabled()) {
                auto source = gluino::claim(gluino::string_cast<char>(_pluginName));
                if (debugConfig.IsAsyncLogging()) {
                    if (debugConfig.GetEventViewerLogger().IsNonBlocking()) {
                        _eventViewerLogger = spdlog::create_async_nb<spdlog::sinks::win_eventlog_sink_mt>(
                                "eventViewerLogger", source);
                    } else {
                        _debuggerLogger = spdlog::create_async<spdlog::sinks::win_eventlog_sink_mt>(
                                "eventViewerLogger", source);
                    }
                } else {
                    _eventViewerLogger = std::make_shared<spdlog::logger>(
                            "eventViewerLogger", std::make_shared<spdlog::sinks::win_eventlog_sink_mt>(source));
                }
                _eventViewerLogger->set_pattern(debugConfig.GetEventViewerLogger().GetLogPattern());
                _eventViewerLogger->set_level(debugConfig.GetEventViewerLogger().GetLogLevel());
                _eventViewerLogger->flush_on(debugConfig.GetEventViewerLogger().GetFlushLevel());
                spdlog::register_logger(_eventViewerLogger);
                if (debugConfig.GetDefaultLogger() == Config::DefaultLogger::eventViewerLogger) {
                    spdlog::set_default_logger(_eventViewerLogger);
                }
            } else {
                _eventViewerLogger = nullptr;
            }

            // Setup file logging.
            _logFile = GetLogFilePath(debugConfig.GetFileLogger().GetLogFile());
            if (debugConfig.GetFileLogger().IsEnabled() && !_logFile.empty()) {
                auto logPathStr = gluino::claim(gluino::string_cast<char>(_logFile));
                if (debugConfig.IsAsyncLogging()) {
                    if (debugConfig.GetDebuggerLogger().IsNonBlocking()) {
                        _fileLogger = spdlog::create_async_nb<spdlog::sinks::rotating_file_sink_mt>(
                                "fileLogger", logPathStr, debugConfig.GetFileLogger().GetMaximumFileSize().ToBytes(),
                                debugConfig.GetFileLogger().GetMaximumFiles(),
                                !(debugConfig.GetFileLogger().IsAppendMode() || reinitializing));
                    } else {
                        _fileLogger = spdlog::create_async<spdlog::sinks::rotating_file_sink_mt>(
                                "fileLogger", logPathStr, debugConfig.GetFileLogger().GetMaximumFileSize().ToBytes(),
                                debugConfig.GetFileLogger().GetMaximumFiles(),
                                !(debugConfig.GetFileLogger().IsAppendMode() || reinitializing));
                    }
                } else {
                    _fileLogger = spdlog::rotating_logger_mt(
                            "fileLogger", logPathStr, debugConfig.GetFileLogger().GetMaximumFileSize().ToBytes(),
                            debugConfig.GetFileLogger().GetMaximumFiles(),
                            !(debugConfig.GetFileLogger().IsAppendMode() || reinitializing));
                }
                _fileLogger->set_pattern(debugConfig.GetFileLogger().GetLogPattern());
                _fileLogger->set_level(debugConfig.GetFileLogger().GetLogLevel());
                _fileLogger->flush_on(debugConfig.GetFileLogger().GetFlushLevel());
                if (debugConfig.GetDefaultLogger() == Config::DefaultLogger::fileLogger) {
                    spdlog::set_default_logger(_fileLogger);
                }
            } else {
                _fileLogger = nullptr;
            }
        }

        [[nodiscard]] FDGE_WIN_STRING_TYPE GetLogFilePath(const std::filesystem::path& logFile) const {
            FDGE_WIN_STRING_TYPE logPath = logFile.template generic_string<FDGE_WIN_CHAR_TYPE>();
            if (logPath.empty()) {
                auto maybeLogPath = SKSE::log::log_directory();
                if (maybeLogPath.has_value()) {
                    logPath = maybeLogPath.value().generic_string<FDGE_WIN_CHAR_TYPE>();
                } else {
                    throw std::logic_error("Unable to determine log file location.");
                }
                logPath += FDGE_WIN_STRING_LITERAL('/');
                logPath += _pluginName;
                logPath += FDGE_WIN_STRING_LITERAL(".log");
            }
            return logPath;
        }

        static inline std::unique_ptr<Logger> _instance;
        std::function<void(bool)> _updateHandler;
        FDGE_WIN_STRING_TYPE _pluginName;
        FDGE_WIN_STRING_TYPE _logFile;
        std::shared_ptr<spdlog::logger> _nullLogger;
        std::shared_ptr<spdlog::logger> _fileLogger;
        std::shared_ptr<spdlog::logger> _debuggerLogger;
        std::shared_ptr<spdlog::logger> _eventViewerLogger;
    };
}
