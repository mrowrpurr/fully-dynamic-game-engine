#pragma once

#include <gluino/ptr.h>

#include "Concepts.h"
#include "PortableID.h"

namespace FDGE::Skyrim {
    class FormIndex {
    public:
        inline FormIndex() noexcept = default;

        FormIndex(const FormIndex&) = delete;

        ~FormIndex() noexcept;

        [[nodiscard]] gluino::optional_ptr<RE::TESForm> operator[](RE::FormID formID) noexcept;

        [[nodiscard]] inline gluino::optional_ptr<const RE::TESForm> operator[](RE::FormID formID) const noexcept {
            auto result = const_cast<FormIndex*>(this)->operator[](formID);
            return *reinterpret_cast<gluino::optional_ptr<const RE::TESForm>*>(&result);
        }

        [[nodiscard]] inline gluino::optional_ptr<RE::TESForm> operator[](std::string_view editorID) noexcept {
            return (*this)[RE::BSFixedString(editorID)];
        }

        [[nodiscard]] inline gluino::optional_ptr<const RE::TESForm> operator[](
                std::string_view editorID) const noexcept {
            auto result = const_cast<FormIndex*>(this)->operator[](editorID);
            return *reinterpret_cast<gluino::optional_ptr<const RE::TESForm>*>(&result);
        }

        [[nodiscard]] gluino::optional_ptr<RE::TESForm> operator[](RE::BSFixedString editorID) noexcept;

        [[nodiscard]] inline gluino::optional_ptr<const RE::TESForm> operator[](
                RE::BSFixedString editorID) const noexcept {
            auto result = const_cast<FormIndex*>(this)->operator[](editorID);
            return *reinterpret_cast<gluino::optional_ptr<const RE::TESForm>*>(&result);
        }

        [[nodiscard]] inline gluino::optional_ptr<RE::TESForm> operator[](const PortableID& portableID) noexcept {
            return Lookup(portableID);
        }

        [[nodiscard]] inline gluino::optional_ptr<const RE::TESForm> operator[](
                const PortableID& portableID) const noexcept {
            auto result = const_cast<FormIndex*>(this)->operator[](portableID);
            return *reinterpret_cast<gluino::optional_ptr<const RE::TESForm>*>(&result);
        }

        [[nodiscard]] inline gluino::optional_ptr<RE::TESForm> operator[](const PortableID&& portableID) noexcept {
            return (*this)[portableID];
        }

        [[nodiscard]] inline gluino::optional_ptr<const RE::TESForm> operator[](
                const PortableID&& portableID) const noexcept {
            return (*this)[portableID];
        }

           [[nodiscard]] inline gluino::optional_ptr<RE::TESForm> Lookup(
                RE::FormID formID, RE::FormType formType = RE::FormType::None) noexcept {
            auto result = (*this)[formID];
            if (result && formType != RE::FormType::None && result->GetFormType() != formType) {
                return {};
            }
            return result;
        }

        [[nodiscard]] inline gluino::optional_ptr<const RE::TESForm> Lookup(
                RE::FormID formID, RE::FormType formType = RE::FormType::None) const noexcept {
            auto result = (*this)[formID];
            if (result && formType != RE::FormType::None && result->GetFormType() != formType) {
                return {};
            }
            return result;
        }

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<T> Lookup(RE::FormID formID) noexcept {
            return reinterpret_cast<T*>(Lookup(formID, T::FORMTYPE).get_raw());
        }

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<const T> Lookup(RE::FormID formID) const noexcept {
            return reinterpret_cast<const T*>(Lookup(formID, T::FORMTYPE).get_raw());
        }

        [[nodiscard]] inline gluino::optional_ptr<RE::TESForm> Lookup(
                std::string_view editorID, RE::FormType formType = RE::FormType::None) noexcept {
            auto result = (*this)[editorID];
            if (result && formType != RE::FormType::None && result->GetFormType() != formType) {
                return {};
            }
            return result;
        }

        [[nodiscard]] inline gluino::optional_ptr<const RE::TESForm> Lookup(
                std::string_view editorID, RE::FormType formType = RE::FormType::None) const noexcept {
            auto result = (*this)[editorID];
            if (result && formType != RE::FormType::None && result->GetFormType() != formType) {
                return {};
            }
            return result;
        }

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<T> Lookup(std::string_view editorID) noexcept {
            return reinterpret_cast<T*>(Lookup(editorID, T::FORMTYPE).get_raw());
        }

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<const T> Lookup(std::string_view editorID) const noexcept {
            return reinterpret_cast<const T*>(Lookup(editorID, T::FORMTYPE).get_raw());
        }

        [[nodiscard]] gluino::optional_ptr<RE::TESForm> Lookup(
                const PortableID& portableID, RE::FormType formType = RE::FormType::None) noexcept;

        [[nodiscard]] inline gluino::optional_ptr<const RE::TESForm> Lookup(
                const PortableID& portableID, RE::FormType formType = RE::FormType::None) const noexcept {
            auto result = const_cast<FormIndex*>(this)->Lookup(portableID, formType);
            return *reinterpret_cast<gluino::optional_ptr<const RE::TESForm>*>(&result);
        }

        [[nodiscard]] inline gluino::optional_ptr<RE::TESForm> Lookup(
                const PortableID&& portableID, RE::FormType formType = RE::FormType::None) noexcept {
            return Lookup(portableID, formType);
        }

        [[nodiscard]] inline gluino::optional_ptr<const RE::TESForm> Lookup(
                const PortableID&& portableID, RE::FormType formType = RE::FormType::None) const noexcept {
            return Lookup(portableID, formType);
        }

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<T> Lookup(const PortableID& portableID) noexcept {
            return reinterpret_cast<T*>(Lookup(portableID, T::FORMTYPE).get_raw());
        }

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<const T> Lookup(
                const PortableID& portableID) const noexcept {
            return reinterpret_cast<const T*>(Lookup(portableID, T::FORMTYPE).get_raw());
        }

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<T> Lookup(const PortableID&& portableID) noexcept {
            return Lookup<T>(portableID);
        }

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<const T> Lookup(
                const PortableID&& portableID) const noexcept {
            return Lookup<T>(portableID);
        }

    private:
        struct State {
            uint32_t Depth{1};
            bool FormIDLock{false};
            bool EditorIDLock{false};
        };

        static thread_local State _currentState;
    };
}
