#pragma once

#include <gluino/enumeration.h>
#include <RE/Skyrim.h>

namespace FDGE::Skyrim {
    class FormType {
    public:
        inline FormType() noexcept = default;

        inline FormType(RE::FormType value) noexcept : _value(value) {
        }

        FormType(std::string_view name);

        [[nodiscard]] std::string_view GetFriendlyName() const noexcept;

        [[nodiscard]] std::string_view GetRecordName() const noexcept;

        [[nodiscard]] inline operator RE::FormType() const noexcept {
            return _value;
        }

    private:
        RE::FormType _value{RE::FormType::None};
    };
}
