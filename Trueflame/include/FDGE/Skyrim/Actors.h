#pragma once

#include <gluino/ptr.h>
#include <RE/Skyrim.h>

namespace FDGE::Skyrim {
    class Actors {
    public:
        class iterator {
        public:
            using iterator_category = std::bidirectional_iterator_tag;
            using difference_type = std::ptrdiff_t;
            using value_type = gluino::safe_ptr<RE::Actor>;
            using pointer = value_type;
            using reference = value_type;

            inline iterator& operator++() noexcept {
                ++_i;
                return *this;
            }

            inline iterator operator++(int) const noexcept {
                return iterator(_i + 1);
            }

            inline iterator& operator--() noexcept {
                --_i;
                return *this;
            }

            inline iterator operator--(int) const noexcept {
                return iterator(_i - 1);
            }

            [[nodiscard]] gluino::optional_ptr<RE::Actor> operator*() const noexcept;

            [[nodiscard]] gluino::optional_ptr<RE::Actor> operator->() const noexcept;

            [[nodiscard]] bool operator==(const iterator& other) const noexcept;

        private:
            inline explicit iterator(std::size_t i) noexcept : _i(i) {
            }

            std::size_t _i{0};

            friend class Actors;
        };

        [[nodiscard]] inline iterator begin() const noexcept {
            return iterator(0);
        }

        [[nodiscard]] iterator end() const noexcept;

        [[nodiscard]] static inline Actors& GetSingleton() noexcept {
            return _singleton;
        }

    private:
        inline Actors() noexcept = default;

        static Actors _singleton;
    };
}
