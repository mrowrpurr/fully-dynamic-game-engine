#pragma once

#include <articuno/articuno.h>
#include <articuno/types/std/iostream.h>

namespace FDGE::Util {
    /**
     * Represents an amount of data.
     *
     * <p>
     * The <code>DataSize</code> class is an amount of bytes, which can be serialized and deserialized via Articuno. The
     * data can be represented by the number of bytes and a suffix which can be metric (e.g. megabytes) or binary (e.g.
     * mibibytes). The serialized representation is useful for configuration files which must specify a data amount.
     * </p>
     */
    class DataSize {
    public:
        using size_type = uint64_t;

        constexpr DataSize() noexcept
                : _bytes(0) {
        }

        constexpr DataSize(size_type bytes) noexcept
                : _bytes(bytes) {
        }

        explicit DataSize(std::string_view string);

        [[nodiscard]] constexpr size_type ToBytes() const noexcept {
            return _bytes;
        }

        [[nodiscard]] constexpr operator size_type() const noexcept {
            return ToBytes();
        }

        [[nodiscard]] inline auto operator<=>(const DataSize& other) const noexcept {
            return _bytes <=> other._bytes;
        }

    protected:
        articuno_serialize(ar) {
            ar <=> articuno::self(std::to_string(*this));
        }

        articuno_deserialize(ar) {
            std::string value;
            ar <=> articuno::self(value);
            *this = DataSize(value);
        }

    private:
        size_type _bytes;

        friend class articuno::access;
    };
}

namespace std {
    std::string to_string(FDGE::Util::DataSize value) noexcept;
}
