#pragma once

#pragma once

#include <mutex>

#pragma warning(push)
#pragma warning(disable: 4251)
/**
 * A timed mutex class made for use across DLL boundaries, equivalent to <code>std::timed_mutex</code>.
 */
namespace FDGE::Util {
    class __declspec(dllexport) TimedMutex {
    public:
        TimedMutex() noexcept;

        ~TimedMutex() noexcept;

        void lock() const noexcept;

        bool try_lock() const noexcept;

        template <class Rep, class Period>
        inline bool try_lock_for(const std::chrono::duration<Rep, Period>& timeout_duration) {
            return try_lock_for_impl(std::chrono::duration_cast<std::chrono::milliseconds>(timeout_duration));
        }

        template <class Clock, class Duration>
        inline bool try_lock_until(const std::chrono::time_point<Clock, Duration>& timeout_time) {
            return try_lock_until_impl(std::chrono::time_point_cast<std::chrono::time_point<std::chrono::system_clock,
                                       std::chrono::milliseconds>>(timeout_time));
        }

        void unlock() const noexcept;

    private:
        bool try_lock_for_impl(const std::chrono::milliseconds& timeout_duration);

        bool try_lock_until_impl(
                const std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>& timeout_time);

        mutable std::timed_mutex _lock;
    };
}
#pragma warning(pop)
