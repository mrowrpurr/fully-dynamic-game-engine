#pragma once

#include <REL/Relocation.h>
#include <SKSE/SKSE.h>

namespace FDGE::Hook {
    template <class Signature>
    class CallHook {
        static_assert("Signature template must be a function signature.");
    };

    template <class Return, class... Args>
    class CallHook<Return(Args...)> {
    public:
        inline CallHook(uint64_t functionID, uint32_t callOffset, Return(*hook)(Args...)) {
            Initialize(REL::ID(functionID).address() + callOffset, hook);
        }

        inline CallHook(REL::ID functionID, uint32_t callOffset, Return(*hook)(Args...)) {
            Initialize(functionID.address() + callOffset, hook);
        }

        inline ~CallHook() {
            uintptr_t base = REL::Module::get().base();
            Logger::Debug("Detaching call hook from address 0x{:X} (offset from image base of 0x{:X} by 0x{:X}...",
                          _address, base, _address - base);
        }

        inline Return operator()(Args... args) const noexcept {
            if constexpr (std::is_void_v<Return>) {
                reinterpret_cast<Return(*)(Args...)>(_original)(args...);
            } else {
                return reinterpret_cast<Return(*)(Args...)>(_original)(args...);
            }
        }

    private:
        void Initialize(uintptr_t address, Return(*hook)(Args...)) {
            _address = address;
            uintptr_t base = REL::Module::get().base();
            Logger::Debug("Attaching call hook to address 0x{:X} (offset from image base of 0x{:X} by 0x{:X}...",
                          address, base, address - base);
            _trampoline.create(64);
            // TODO: Check if to do 6 byte call.
            _original = _trampoline.write_call<5>(address, hook);
        }

        SKSE::Trampoline _trampoline{"FuDGE Call Hook"};
        uintptr_t _address;
        uintptr_t _original;
    };
}
