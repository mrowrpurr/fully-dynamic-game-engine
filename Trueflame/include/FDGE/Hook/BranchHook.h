#pragma once

#include <REL/Relocation.h>
#include <SKSE/SKSE.h>

namespace FDGE::Hook {
    template <class Signature>
    class BranchHook {
        static_assert("Signature template must be a function signature.");
    };

    template <class Return, class... Args>
    class BranchHook<Return(Args...)> {
    public:
        inline BranchHook(uint64_t functionID, uint32_t callOffset, Return(*hook)(Args...)) {
            Initialize(REL::ID(functionID).address() + callOffset, hook);
        }

        inline BranchHook(REL::ID functionID, uint32_t callOffset, Return(*hook)(Args...)) {
            Initialize(functionID.address() + callOffset, hook);
        }

        inline ~BranchHook() {
            uintptr_t base = REL::Module::get().base();
            Logger::Debug("Detaching branch hook from address 0x{:X} (offset from image base of 0x{:X} by 0x{:X}...",
                          _address, base, _address - base);
        }

        inline Return operator()(Args... args) const noexcept {
            if constexpr (std::is_void_v<Return>) {
                reinterpret_cast<Return(*)(Args...)>(_original)(args...);
            } else {
                return reinterpret_cast<Return(*)(Args...)>(_original)(args...);
            }
        }

    private:
        void Initialize(uintptr_t address, Return(*hook)(Args...)) {
            _address = address;
            uintptr_t base = REL::Module::get().base();
            Logger::Debug("Attaching branch hook to address 0x{:X} (offset from image base of 0x{:X} by 0x{:X}...",
                          address, base, address - base);
            _trampoline.create(64);
            // TODO: Check if to do 6 byte branch.
            _original = _trampoline.write_branch<5>(address, hook);
        }

        SKSE::Trampoline _trampoline{"FuDGE Branch Hook"};
        uintptr_t _address;
        uintptr_t _original;
    };
}
