#pragma once

#include <cstdint>
#include <cstdlib>
#include <stdexcept>

#include <REL/Relocation.h>

#include "../Logger.h"

namespace FDGE::Hook {
    class AddressID {
    public:
        constexpr AddressID() noexcept = default;

        constexpr AddressID(uint64_t aeID, uint64_t seID = 0, uint64_t vrID = 0) noexcept :
                _aeID(aeID), _seID(seID), _vrID(vrID) {
        }

        [[nodiscard]] constexpr static AddressID Create(uint64_t aeID, uint64_t seID = 0, uint64_t vrID = 0) noexcept {
            return AddressID(aeID, seID, vrID);
        }

        [[nodiscard]] constexpr static AddressID CreateWithoutAE(uint64_t seID, uint64_t vrID = 0) noexcept {
            return AddressID(0, seID, vrID);
        }

        [[nodiscard]] constexpr static AddressID CreateWithoutSE(uint64_t aeID, uint64_t vrID = 0) noexcept {
            return AddressID(aeID, 0, vrID);
        }

        [[nodiscard]] inline operator REL::ID() const noexcept {
            return GetID();
        }

        [[nodiscard]] inline std::uintptr_t address() const noexcept {
            return GetID().address();
        }

        [[nodiscard]] inline std::uint64_t id() const noexcept {
            return GetID().id();
        }

        [[nodiscard]] std::size_t offset() const {
            return GetID().offset();
        }

    private:
        [[nodiscard]] static bool IsAE() noexcept;

        [[nodiscard]] inline const REL::ID& GetID() const {
            REL::ID result;
#ifdef FDGE_SKYRIM_VR
            result = _vrID;
#else
            result = IsAE() ? _aeID : _seID;
#endif
            if (result.id() == 0) {
                Logger::Critical("This SKSE plugin does not support the Skyrim version are currently using.");
                throw std::logic_error("This SKSE plugin does not support the Skyrim version are currently using.");
            }
            return result;
        }

        REL::ID _seID;
        REL::ID _aeID;
        REL::ID _vrID;
    };
}
