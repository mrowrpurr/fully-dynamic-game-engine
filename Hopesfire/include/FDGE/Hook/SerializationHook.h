#pragma once

#include <atomic>
#include <latch>

#include <FDGE/Hook/Serialization.h>
#include <FDGE/Util/EventSource.h>

namespace FDGE::Hook {
    class SerializationHook {
    protected:
        SerializationHook(std::string_view name);

        SerializationHook(std::string&& name);

        ~SerializationHook() noexcept;

        virtual inline void OnNewGame() {
        }

        virtual inline void OnGameSaving([[maybe_unused]] std::ostream& out) {
        }

        virtual inline void OnGameSaved([[maybe_unused]] std::ostream& out) {
        }

        virtual inline void OnGameLoading([[maybe_unused]] std::istream& in) {
        }

        virtual inline void OnGameLoaded([[maybe_unused]] std::istream& in) {
        }

    private:
        void Initialize();

        std::string _name;
        FDGE::Util::EventRegistration _listener;
    };
}
