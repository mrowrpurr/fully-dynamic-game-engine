#include <FDGE/Hook/SerializationHook.h>

#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Hook;

SerializationHook::SerializationHook(std::string_view name) : _name(name.data()) {
    Initialize();
}

SerializationHook::SerializationHook(std::string&& name) : _name(name.data()) {
    Initialize();
}

SerializationHook::~SerializationHook() noexcept {
    UnregisterSaveHandler(_name, HandlerStage::Pre);
    UnregisterSaveHandler(_name, HandlerStage::Post);
    UnregisterLoadHandler(_name, HandlerStage::Pre);
    UnregisterLoadHandler(_name, HandlerStage::Post);
}

void SerializationHook::Initialize() {
    RegisterSaveHandler(_name, HandlerStage::Pre, [this](std::ostream& out) {
        OnGameSaving(out);
    });
    RegisterSaveHandler(_name, HandlerStage::Post, [this](std::ostream& out) {
        OnGameSaved(out);
    });
    RegisterLoadHandler(_name, HandlerStage::Pre, [this](std::istream& in) {
        OnGameLoading(in);
    });
    RegisterLoadHandler(_name, HandlerStage::Post, [this](std::istream& in) {
        OnGameLoaded(in);
    });
    _listener = System().Listen([this](const NewGameEvent&) {
        OnNewGame();
    });
}
